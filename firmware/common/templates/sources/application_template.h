/// @file

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#include "application.h"

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*!
 * @brief Configures Application
 * @param p_configs Address of structure holding input configurations: list of queues to exchange data and list of tasks to notify
 * @param time Internal value of time for task self activation (if any)
 */
void name_app_cfg(uint32_t time);

/*!
 * @brief Generates Application main task
 * @param p_handle Address of task handle of this application
 * @param task_prio Priority number for main task of this application
 */
void name_app_run(TaskHandle_t *p_handle, uint32_t task_prio);

/*!
 * @brief Performs a request to this application
 * @param type Request type to perform (in case data is passed, should be an structure address, example is given on source)
 * @param timeout_ms Timeout to perform request (if no data is expected in return should be zero, 0)
 * @return Status of the operation
 */
bool name_app_req(type2_t type, uint32_t timeout_ms);

#ifdef __cplusplus
}
#endif /*  __cplusplus */
