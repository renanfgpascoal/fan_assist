///@file main.c

/**************************************************************************************************
 * PRIVATE DEFINES
 *************************************************************************************************/

/* Enablers/disablers for a set of tests */
/* To disable a specific test (usually broken) zero it's respective if */

/* Enables running ALL lib tests */
/* To run a single set of tests, disable it */
#define RUN_ALL_LIB_TESTSx

#ifdef RUN_ALL_LIB_TESTS

/* Enables running of demo lib tests */
#define RUN_DEMO_TESTS

#else
/* Put single lib tests runner here */

#endif

/**************************************************************************************************
 * INCLUDES
 *************************************************************************************************/

#include <stdio.h>
#include "tests_framework.h"

/* Unit Test file header files inclusion start */

/* Unit Test file header files inclusion end */

/* Tests specific header files start */

#ifdef RUN_DEMO_TESTS
#include "demo_lib-tests.h"
#endif /* RUN_BUFFER_CONTROL_TEST */

/* Tests specific header files start */

/**************************************************************************************************
 * EXTERN FUNCTIONS
 *************************************************************************************************/

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/
/* Auxiliary functions start */

/* Auxiliary functions end */

int main(void) {
	UNITY_BEGIN();

#ifdef RUN_DEMO_TESTS

	CONSOLE_TRACE(LOG_WARN, "\n----- Demo lib Tests ----\n");

	if (1) { TEST_RUN(demo_tests); }

#endif /* RUN_DEMO_TESTS */

	return UNITY_END();
}
