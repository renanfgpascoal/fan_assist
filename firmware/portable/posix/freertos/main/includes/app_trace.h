/// @file app_trace.h

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */
#include "tracealyzer_hooks.h"

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/* Post includes defines start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#define TRACE_START()	\
	do{	\
		vTraceSetFrequency(configTICK_RATE_HZ);	\
		vTraceEnable(TRC_START);	\
	}while(0)

#define TRACE_INIT()	\
	do{	\
		vTraceSetFrequency(configTICK_RATE_HZ);	\
		vTraceEnable(TRC_INIT);	\
	}while(0)

#define TRACE_PRINT(...)	\
	vTracePrintF(xTraceRunning, __VA_ARGS__)

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Post includes defines end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#ifdef __cplusplus
}
#endif /*  __cplusplus */
