///@file run-time-stats-utils.c

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#include <time.h>
#include "FreeRTOS.h"

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#include "run-time-stats-utils.h"

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

/* Time at start of day (in ms). */
static unsigned long ulStartTimeMs = 0;

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void vConfigureTimerForRunTimeStats( void )
{
	struct timespec xNow;

	clock_gettime(CLOCK_MONOTONIC, &xNow);
	ulStartTimeMs = xNow.tv_sec * 1000 + xNow.tv_nsec / 1000000;
}

unsigned long ulGetRunTimeCounterValue( void )
{
	struct timespec xNow;

	/* Time at start. */
	clock_gettime(CLOCK_MONOTONIC, &xNow);
	return (xNow.tv_sec * 1000 + xNow.tv_nsec / 1000000) - ulStartTimeMs;
}

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */
