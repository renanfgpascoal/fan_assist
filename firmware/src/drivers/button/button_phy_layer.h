/// @file button_phy_layer.h

#pragma once

#ifdef __cplusplus
extern "C" {
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include "gpio_hal_driver.h"

/* Module functioning includes end */

/* Post includes defines start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Post includes defines end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*! Enumeration for button physical layer error types */
typedef enum button_phy_err_t {
	/*! Success */
	BUTTON_PHY_OK = 0,
	/*! Failure */
	BUTTON_PHY_FAIL = 1,
	/*! Maximum error types */
	BUTTON_PHY_ERR_MAX
} button_phy_err_t;

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*!
 * @brief Button physical layer setup function
 * @return BUTTON_PHY_OK if success, otherwise error status
 */
button_phy_err_t button_phy_setup(void);

#ifdef __cplusplus
}
#endif /*  __cplusplus */
