/// @file pwm_esp32_interface.h

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include "driver/pwm.h"

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/* Post includes defines start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Post includes defines end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*! Interface for PWM Hardware Abstraction Layer (HAL) pin configuration structure */
typedef void pwm_hal_pin_config_t;

/*! Interface for PWM Hardware Abstraction Layer (HAL) configuration structure */
typedef struct pwm_hal_config_t {
	uint32_t period_ms;
	uint32_t *p_duties;
	float *p_phases;
	uint32_t *p_pin;
	uint32_t n_of_channels;
} pwm_hal_config_t;

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*! Enumeration for PWM Hardware Abstraction Layer (HAL) error types */
typedef enum pwm_hal_err_t {
	/*! Success */
	PWM_HAL_OK = ESP_OK,
	/*! Failure */
	PWM_HAL_FAIL = ESP_FAIL,
	/*! Invalid argument */
	PWM_HAL_INVALID_ARG = ESP_ERR_INVALID_ARG,
	/*! Maximum error types */
	PWM_HAL_ERR_MAX
} pwm_hal_err_t;

/*! Enumeration for PWM Hardware Abstraction Layer (HAL) duty cycle operation modes */
typedef enum pwm_hal_channel_t {
	/*! PWM channel 0 */
	PWM_CHANNEL_0 = 0,
	/*! PWM channel 1 */
	PWM_CHANNEL_1 = 1,
	/*! PWM channel 2 */
	PWM_CHANNEL_2 = 2,
	/*! PWM channel 3 */
	PWM_CHANNEL_3 = 3,
	/*! Maximum PWM channels */
	PWM_CHANNEL_MAX
} pwm_hal_channel_t;

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*!
 * @brief	Interface for PWM Hardware Abstraction Layer (HAL) get period function
 * @param CHANNEL		PWM channel number
 * @return Current PWM base period in ms
 */
#define PWM_HAL_GET_PERIOD(CHANNEL)	\
		pwm_esp8266_get_period()

/*!
 * @brief	Interface for PWM Hardware Abstraction Layer (HAL) set duty cycle function
 * @param CHANNEL		PWM channel number
 * @param DUTY_US		Duty cycle values in microseconds (us)
 * @return PWM_HAL_OK if success, otherwise error status
 */
#define PWM_HAL_SET_DUTY(CHANNEL, DUTY_US)	\
	pwm_esp8266_set_duty((CHANNEL), \
	                     (DUTY_US))

/*!
 * @brief	Interface for PWM Hardware Abstraction Layer (HAL) initialization function
 * @param P_CONFIG	Address of structure holding PWM configuration data
 * @return PWM_HAL_OK if success, otherwise error status
 */
#define PWM_HAL_INIT(P_CONFIG)	\
	pwm_esp8266_init((P_CONFIG))

/*!
 * @brief	Interface for PWM Hardware Abstraction Layer (HAL) deinitialization function
 * @return PWM_HAL_OK if success, otherwise error status
 */
#define PWM_HAL_DEINIT()	\
	pwm_esp8266_deinit()

uint32_t pwm_esp8266_get_period(void);
pwm_hal_err_t pwm_esp8266_set_duty(pwm_hal_channel_t channel, uint32_t duty_us);
pwm_hal_err_t pwm_esp8266_init(pwm_hal_config_t *p_config);
pwm_hal_err_t pwm_esp8266_deinit(void);

#ifdef __cplusplus
}
#endif /*  __cplusplus */
