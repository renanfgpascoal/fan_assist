/// @file gpio_esp8266_interface.h

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*!
 * @brief Encode bit mask for pin selection
 */
#define GPIO_HAL_SEL(PIN)	\
	(1UL << PIN)

/*!
 * @brief Interface for GPIO Hardware Abstraction Layer (HAL) configuration
 * @param PIN_MASK		Bit mask of selected pins
 * @param MODE				Pin mode operation
 * @param PULL_UP			Pull up enable
 * @param PULL_DOWN		Pull down enable
 * @param INT_TYPE		Pin interruption type
 */
#define GPIO_HAL_CONFIG(PIN_MASK, MODE, PULL_UP, PULL_DOWN, INT_TYPE)	\
	{	\
		.pin_bit_mask = (PIN_MASK),	\
		.mode = (MODE),	\
		.pull_up_en = (PULL_UP),	\
		.pull_down_en = (PULL_DOWN),	\
		.intr_type = (INT_TYPE),	\
	}

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include "driver/gpio.h"

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/* Post includes defines start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Post includes defines end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*! Interface for GPIO Hardware Abstraction Layer (HAL) ISR function type */
typedef gpio_isr_t gpio_hal_isr_t;

/*! Interface for GPIO Hardware Abstraction Layer (HAL) pin number enumeration */
typedef gpio_num_t gpio_hal_pin_num_t;

/*! Interface for GPIO Hardware Abstraction Layer (HAL) configuration structure */
typedef gpio_config_t gpio_hal_config_t;

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*! Enumeration for GPIO Hardware Abstraction Layer (HAL) error types */
typedef enum gpio_hal_err_t {
	/*! Success */
	GPIO_HAL_OK = ESP_OK,
	/*! Failure */
	GPIO_HAL_FAIL = ESP_FAIL,
	/*! Invalid argument */
	GPIO_HAL_INVALID_ARG = ESP_ERR_INVALID_ARG,
	/*! Maximum error types */
	GPIO_HAL_ERR_MAX
} gpio_hal_err_t;

/*! Enumeration for GPIO Hardware Abstraction Layer (HAL) duty cycle operation modes */
typedef enum gpio_hal_pin_level_t {
	/*! Pin low level*/
	GPIO_PIN_LOW = 0,
	/*! Pin high level*/
	GPIO_PIN_HIGH = 1,
	/*! Maximum pin levels */
	GPIO_PIN_LEVEL_MAX
} gpio_hal_pin_level_t;

/*! Enumeration for GPIO Hardware Abstraction Layer (HAL) interruption types */
typedef enum gpio_hal_int_types_t {
	/*! Interrupt disable */
	GPIO_INT_DISABLE = GPIO_INTR_DISABLE,
	/*! Interrupt on positive edge */
	GPIO_INT_POSEDGE = GPIO_INTR_POSEDGE,
	/*! Interrupt on negative edge */
	GPIO_INT_NEGEDGE = GPIO_INTR_NEGEDGE,
	/*! Interrupt on any edge */
	GPIO_INT_ANYEDGE = GPIO_INTR_ANYEDGE,
	/*! Interrupt on low level input */
	GPIO_INT_LOW_LEVEL = GPIO_INTR_LOW_LEVEL,
	/*! Interrupt on high level input */
	GPIO_INT_HIGH_LEVEL = GPIO_INTR_HIGH_LEVEL,
	/*! Maximum interrupts types */
	GPIO_INT_TYPES_MAX
} gpio_hal_int_types_t;

/*! Enumeration for GPIO Hardware Abstraction Layer (HAL) operation modes */
typedef enum gpio_hal_modes_t {
	/*! Disable input and output operation */
	GPIO_HAL_MODE_DISABLE = GPIO_MODE_DISABLE,
	/*! Only input operation */
	GPIO_HAL_MODE_INPUT = GPIO_MODE_INPUT,
	/*! Only output operation */
	GPIO_HAL_MODE_OUTPUT = GPIO_MODE_OUTPUT,
	/*! Maximum operation modes */
	GPIO_MODES_MAX
} gpio_hal_modes_t;

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*!
 * @brief	Interface for GPIO Hardware Abstraction Layer (HAL) get level function
 * @param PIN		GPIO number
 * @return Current pin level, 1 = high, or 0 = low
 */
#define GPIO_HAL_GET_LEVEL(PIN)	\
	gpio_get_level((gpio_num_t)(PIN))

/*!
 * @brief	Interface for GPIO Hardware Abstraction Layer (HAL) set level function
 * @param PIN			GPIO number
 * @param LEVEL		1 = high, or 0 = low
 * @return GPIO_HAL_OK if success, otherwise error status
 */
#define GPIO_HAL_SET_LEVEL(PIN, LEVEL)	\
	(gpio_hal_err_t)gpio_set_level((gpio_num_t)(PIN), (LEVEL))

/*!
 * @brief	Interface for GPIO Hardware Abstraction Layer (HAL) ISR addition function
 * @param P_CONFIG	Address of structure holding GPIO pin configuration data
 * @return GPIO_HAL_OK if success, otherwise error status
 */
#define GPIO_HAL_ISR_ADD(PIN, ISR_FUNC, P_ARGS)	\
	(gpio_hal_err_t)gpio_isr_handler_add((gpio_num_t)(PIN), (gpio_isr_t)(ISR_FUNC), (void *)(P_ARGS))

/*!
 * @brief	Interface for GPIO Hardware Abstraction Layer (HAL) ISR setup function
 * @param P_CONFIG	Address of structure holding GPIO pin configuration data
 * @return GPIO_HAL_OK if success, otherwise error status
 */
#define GPIO_HAL_ISR_SETUP()	\
	(gpio_hal_err_t)gpio_install_isr_service(0)

/*!
 * @brief	Interface for GPIO Hardware Abstraction Layer (HAL) setup function
 * @param P_CONFIG	Address of structure holding GPIO pin configuration data
 * @return GPIO_HAL_OK if success, otherwise error status
 */
#define GPIO_HAL_SETUP(P_CONFIG)	\
	(gpio_hal_err_t)gpio_config((const gpio_config_t *)(P_CONFIG))

#ifdef __cplusplus
}
#endif /*  __cplusplus */
