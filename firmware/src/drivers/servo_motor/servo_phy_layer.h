/// @file servo_phy_layer.h

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*! Maximum Jig position in percentage */
#define SERVO_PHY_JIG_POS_MAX	(57)

/*! Minimum Jig position in percentage */
#define SERVO_PHY_JIG_POS_MIN	(30)

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include "pwm_hal.h"

/* Module functioning includes end */

/* Post includes defines start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Post includes defines end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

typedef struct servo_phy_continuous_params_t {
	uint32_t speed_pct;
	uint32_t direction;
	uint32_t time_ms;
} servo_phy_continuous_params_t;

/*! Enumeration for servo motor physical layer error types */
typedef enum servo_phy_err_t {
	/*! Success */
	SERVO_PHY_OK = 0,
	/*! Failure */
	SERVO_PHY_FAIL,
	/*! Invalid parameter */
	SERVO_PHY_INVALID_PARAM,
	/*! PWM not running */
	SERVO_PHY_NOT_RUNNING,
	/*! Maximum error types */
	SERVO_PHY_ERR_MAX
} servo_phy_err_t;

/*! Enumeration for PWM Hardware Abstraction Layer (HAL) duty cycle operation modes */
typedef enum servo_phy_num_t {
	/*! PWM channel of servo motor 0 */
	SERVO_0 = PWM_CHANNEL_0,
	/*! PWM channel of servo motor 1 */
	SERVO_1 = PWM_CHANNEL_1,
	/*! PWM channel of servo motor 2 */
	SERVO_2 = PWM_CHANNEL_2,
	/*! PWM channel of servo motor 3 */
	SERVO_3 = PWM_CHANNEL_3,
	/*! Maximum servo motor numbers */
	SERVO_NUM_MAX
} servo_phy_num_t;

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*!
 * @brief Servo motor physical layer set position function
 * @param from	Servo motor number
 * @param to		Duty cycle in percentage
 * @return SERVO_PHY_OK if success, otherwise error status
 */
servo_phy_err_t servo_phy_set_pos(servo_phy_num_t from, uint32_t to);

servo_phy_err_t servo_phy_set_rot(servo_phy_num_t from,
                                  servo_phy_continuous_params_t *p_to);

/*!
 * @brief Servo motor physical layer setup function
 * @return SERVO_PHY_OK if success, otherwise error status
 */
servo_phy_err_t servo_phy_setup();

#ifdef __cplusplus
}
#endif /*  __cplusplus */
