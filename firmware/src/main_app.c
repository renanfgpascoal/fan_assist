///@file main.c

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include "main_app.h"
#include "application.h"
#include "fan_control_application.h"
#include "soft_wdt_application.h"
#include "network_mgr_application.h"
#include "proprietary_utils.h"

#include "button_phy_layer.h"

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

static void app_running_check(void);

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

static app_info_t app_info_list[APP_MAX_INDEXES];

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */
#include "driver/gpio.h"

void app_start(void *pvParameters) {
	gpio_config_t io_conf;
	//disable interrupt
	io_conf.intr_type = GPIO_INTR_DISABLE;
	//set as output mode
	io_conf.mode = GPIO_MODE_OUTPUT;
	//bit mask of the pins that you want to set,e.g.GPIO15/16
	io_conf.pin_bit_mask = GPIO_Pin_16;
	//disable pull-down mode
	io_conf.pull_down_en = 0;
	//disable pull-up mode
	io_conf.pull_up_en = 0;
	//configure GPIO with the given settings
	gpio_config(&io_conf);
	(void) button_phy_setup();

	soft_wdt_app_cfg(app_info_list, MS_FROM_SECONDS(5));
	soft_wdt_app_run();

	app_info_list[NETWORK_MGR_APP_IDX].is_critical = true;
	app_info_list[NETWORK_MGR_APP_IDX].killed_times = 0;
	app_info_list[FAN_CONTROL_APP_IDX].is_critical = true;
	app_info_list[FAN_CONTROL_APP_IDX].killed_times = 0;

	network_mgr_app_cfg(0);
	network_mgr_app_run();
	fan_control_app_cfg(2000);
	fan_control_app_run();

	app_running_check();

	return;
}

static void app_running_check(void) {
	APP_LOG(LOG_INFO, FMT_NONE("app_running_check"));
//	servo_phy_continuous_params_t to;
	for (;;) {
		gpio_set_level(GPIO_NUM_16, 1);
//		to.speed_pct = 100;
//		to.direction = 1;
//		to.time_ms = 1000;
//		servo_phy_set_rot(SERVO_0, &to);
//		servo_phy_set_rot(SERVO_1, &to);

		RELATIVE_DELAY(500);

		gpio_set_level(GPIO_NUM_16, 0);

//		to.direction = 0;
//		servo_phy_set_rot(SERVO_0, &to);
//		servo_phy_set_rot(SERVO_1, &to);

		RELATIVE_DELAY(500);

		network_mgr_app_cfg(0);
		network_mgr_app_run();

		RELATIVE_DELAY(MS(100));
	}

	return;
}
