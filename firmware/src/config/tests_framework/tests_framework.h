/// @file

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#if defined (UNITY_FRAMEWORK)

/*! Unit tests funcion signature for Unity Unit Tests Framework  */
#define TEST_FUNCTION(NAME)					void NAME(void)

/*! Unit tests Runner function / macro for Unity Unit Tests Framework  */
#define TEST_RUN(FUNCTION)					RUN_TEST(FUNCTION)

/*! Unit tests integer equality assertion for Unity Unit Tests Framework  */
#define TEST_ASSERT_INT_EQUAL(FROM, TO)		TEST_ASSERT_EQUAL(FROM, TO)

#elif defined (CMOCKA_FRAMEWORK)

/* Unit tests funcion signature for Cmocka Unit Tests Framework  */
#define TEST_FUNCTION(NAME)					void NAME(void **state)

/*! Unit tests Runner function / macro for Cmocka Unit Tests Framework  */
#define TEST_RUN(FUNCTION)

/*! Unit tests integer equality assertion for Cmocka Unit Tests Framework  */
#define TEST_ASSERT_INT_EQUAL(FROM, TO)		assert_int_equal(FROM, TOO)

#elif defined (CATCH2_FRAMEWORK)

/*! Unit tests funcion signature for Catch2 Unit Tests Framework  */
#define TEST_FUNCTION(NAME)					void NAME(void)

/*! Unit tests Runner function / macro for Catch2 Unit Tests Framework  */
#define TEST_RUN(FUNCTION)

/*! Unit tests integer equality assertion for Catch2 Unit Tests Framework  */
#define TEST_ASSERT_INT_EQUAL(FROM, TO)

#else /* DEFAULT*/

/*! Default unit tests funcion signature: not known test framework */
#define TEST_FUNCTION(NAME)					void NAME(void)

/*! Unit tests Runner function / macro for Default Unit Tests Framework  */
#define TEST_RUN(FUNCTION)

/*! Unit tests integer equality assertion Default */
#define TEST_ASSERT_INT_EQUAL(FROM, TO)

#endif /* DEFAULT */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#if defined (UNITY_FRAMEWORK)

#include "unity.h"

#elif defined (CMOCKA_FRAMEWORK)

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>

#include "cmocka.h"

#elif defined (CATCH2_FRAMEWORK)

#include "catch.h"

#endif /* CATCH2_FRAMEWORK */

/* System functioning includes end */

/* Module functioning includes start */

#include "application.h"

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef CMOCKA_FRAMEWORK

/* SHALL BE CALLED IN EACH TEST FUNCTION */

/* @brief Function to remove CMOCKA Unit Tests Framework compiler warnings due to unused variable state */
/* @params state Variable potetially unused in unit tests */
void kill_warnings(void **state);

#elif defined (UNITY_FRAMEWORK)

#endif /* CMOCKA_FRAMEWORK */

#ifdef __cplusplus
}
#endif /*  __cplusplus */

