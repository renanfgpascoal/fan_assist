/// @file

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include "driver/gpio.h"
#include "hw_support.h"

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/* Post includes defines start */

#ifndef HW_VERSION

/*! Defines Hardware version for project */
#define HW_VERSION  \
		HW_SUPPORT(HW_SUPPORT_COMM_WIFI \
		)

#endif

/*! Hardware demo specific Pin */
#define DEMO_SPECIFIC_PIN            0

/*! LoRa Radio SCLK pin value */

/* Post includes defines end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

#define PWM_0_PIN					GPIO_NUM_12
#define PWM_1_PIN					GPIO_NUM_13

#define BUTTON_0_PIN			GPIO_NUM_4
#define BUTTON_1_PIN			GPIO_NUM_5

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#ifdef __cplusplus
}
#endif /*  __cplusplus */

