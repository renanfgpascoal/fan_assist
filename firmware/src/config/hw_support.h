/// @file

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*! Hardware support bit field for WIFI */
#define	HW_SUPPORT_WIFI_BIT							0

/*! Enabled hardware support for WIFI */
#define	HW_SUPPORT_COMM_WIFI 					(1 << (HW_SUPPORT_WIFI_BIT))

/*!
 * @brief Defines hardware support for a given board version
 * @param WIFI Wifi type supported
 */
#define HW_SUPPORT(WIFI) \
	((WIFI) | (0 << 1))

/*!
 * @brief Checks if a given board supports WIFI
 * @param VALUE Hardware descriptor
 */
#define IS_WIFI_SUPPORTED(VALUE)        ((VALUE) & (HW_SUPPORT_COMM_WIFI))

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#ifdef __cplusplus
}
#endif /*  __cplusplus */

