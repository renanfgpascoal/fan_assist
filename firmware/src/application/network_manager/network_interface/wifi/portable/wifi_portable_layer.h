/// @file wifi_portable_layer.h

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include "application.h"

#ifdef APP_TRACE_POSIX

#else

#include "esp_wifi.h"
#include "esp_netif.h"

#endif /* APP_TRACE_POSIX */

/* Module functioning includes end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

#ifdef APP_TRACE_POSIX
/*! WiFi stack initial configuration DUMMY parameters */
typedef struct wifi_init_config_dummy_t{
    void*									 event_handler;
    void*                  osi_funcs;
    uint8_t                qos_enable;
    uint8_t                ampdu_rx_enable;
    uint8_t                rx_ba_win;
    uint8_t                rx_ampdu_buf_num;
    uint32_t               rx_ampdu_buf_len;
    uint32_t               rx_max_single_pkt_len;
    uint32_t               rx_buf_len;
    uint8_t                amsdu_rx_enable;
    uint8_t                rx_buf_num;
    uint8_t                rx_pkt_num;
    uint8_t                left_continuous_rx_buf_num;
    uint8_t                tx_buf_num;
    uint8_t                nvs_enable;
    uint8_t                nano_enable;
    uint8_t                wpa3_sae_enable;
    uint32_t               magic;
} wifi_init_config_dummy_t;

typedef wifi_init_config_dummy_t wifi_init_config_t;

/*! WiFi mode DUMMY values */
typedef enum wifi_mode_dummy_t{
		WIFI_MODE_NULL = 0,
		WIFI_MODE_STA,
		WIFI_MODE_AP,
		WIFI_MODE_APSTA,
		WIFI_MODE_MAX
} wifi_mode_dummy_t;

typedef wifi_mode_dummy_t wifi_mode_t;

/*! WiFi interface DUMMY values */
typedef enum wifi_interface_dummy_t{
		ESP_IF_WIFI_STA = 0,
		ESP_IF_WIFI_AP,
		ESP_IF_MAX
} wifi_interface_dummy_t;

typedef wifi_interface_dummy_t wifi_interface_t;

typedef enum {
    WIFI_AUTH_OPEN = 0,         /**< authenticate mode : open */
    WIFI_AUTH_WEP,              /**< authenticate mode : WEP */
    WIFI_AUTH_WPA_PSK,          /**< authenticate mode : WPA_PSK */
    WIFI_AUTH_WPA2_PSK,         /**< authenticate mode : WPA2_PSK */
    WIFI_AUTH_WPA_WPA2_PSK,     /**< authenticate mode : WPA_WPA2_PSK */
    WIFI_AUTH_WPA2_ENTERPRISE,  /**< authenticate mode : WPA2_ENTERPRISE */
    WIFI_AUTH_WPA3_PSK,         /**< authenticate mode : WPA3_PSK */
    WIFI_AUTH_WPA2_WPA3_PSK,    /**< authenticate mode : WPA2_WPA3_PSK */
    WIFI_AUTH_MAX
} wifi_auth_mode_t;

/*! WiFi soft-AP DUMMY values */
typedef struct wifi_ap_config_dummy_t{
    uint8_t ssid[32];
    uint8_t password[64];
    uint8_t ssid_len;
    uint8_t channel;
    uint32_t authmode;
    uint8_t ssid_hidden;
    uint8_t max_connection;
    uint16_t beacon_interval;
} wifi_ap_config_dummy_t;

typedef wifi_ap_config_dummy_t wifi_ap_config_t;

/*! WiFi station DUMMY values */
typedef struct wifi_sta_config_dummy_t{
    uint8_t ssid[32];
    uint8_t password[64];
    uint32_t scan_method;
    bool bssid_set;
    uint8_t bssid[6];
    uint8_t channel;
    uint16_t listen_interval;
    uint32_t sort_method;
    uint32_t  threshold;
    uint32_t pmf_cfg;
    uint32_t rm_enabled:1;
    uint32_t btm_enabled:1;
    uint32_t reserved:30;
} wifi_sta_config_dummy_t;

typedef wifi_sta_config_dummy_t wifi_sta_config_t;

/*! WiFi configuration DUMMY values */
typedef union wifi_config_dummy_t{
    wifi_ap_config_t  ap;
    wifi_sta_config_t sta;
} wifi_config_dummy_t;

typedef wifi_config_dummy_t wifi_config_t;
typedef enum {
    TCPIP_ADAPTER_IF_STA = 0,     /**< Wi-Fi STA (station) interface */
    TCPIP_ADAPTER_IF_AP,          /**< Wi-Fi soft-AP interface */
    TCPIP_ADAPTER_IF_ETH,         /**< Ethernet interface */
    TCPIP_ADAPTER_IF_TEST,        /**< tcpip stack test interface */
    TCPIP_ADAPTER_IF_MAX
} tcpip_adapter_if_t;

struct ip4_addr {
  uint32_t addr;
};

typedef struct ip4_addr ip4_addr_t;

typedef struct {
    ip4_addr_t ip;              /**< TCP-IP adatpter IPV4 addresss */
    ip4_addr_t netmask;         /**< TCP-IP adatpter IPV4 netmask */
    ip4_addr_t gw;              /**< TCP-IP adatpter IPV4 gateway */
} tcpip_adapter_ip_info_t;

typedef struct {
    tcpip_adapter_if_t if_index;        /*!< Interface for which the event is received */
    tcpip_adapter_ip_info_t ip_info;    /*!< IP address, netmask, gatway IP address */
    bool ip_changed;                    /*!< Whether the assigned IP has changed or not */
} ip_event_got_ip_t;

typedef enum {
    WIFI_SECOND_CHAN_NONE = 0,  /**< the channel width is HT20 */
    WIFI_SECOND_CHAN_ABOVE,     /**< the channel width is HT40 and the second channel is above the primary channel */
    WIFI_SECOND_CHAN_BELOW,     /**< the channel width is HT40 and the second channel is below the primary channel */
} wifi_second_chan_t;

typedef enum {
    WIFI_CIPHER_TYPE_NONE = 0,   /**< the cipher type is none */
    WIFI_CIPHER_TYPE_WEP40,      /**< the cipher type is WEP40 */
    WIFI_CIPHER_TYPE_WEP104,     /**< the cipher type is WEP104 */
    WIFI_CIPHER_TYPE_TKIP,       /**< the cipher type is TKIP */
    WIFI_CIPHER_TYPE_CCMP,       /**< the cipher type is CCMP */
    WIFI_CIPHER_TYPE_TKIP_CCMP,  /**< the cipher type is TKIP and CCMP */
    WIFI_CIPHER_TYPE_AES_CMAC128,/**< the cipher type is AES-CMAC-128 */
    WIFI_CIPHER_TYPE_UNKNOWN,    /**< the cipher type is unknown */
} wifi_cipher_type_t;

typedef enum {
    WIFI_ANT_ANT0,          /**< WiFi antenna 0 */
    WIFI_ANT_ANT1,          /**< WiFi antenna 1 */
    WIFI_ANT_MAX,           /**< Invalid WiFi antenna */
} wifi_ant_t;

typedef enum {
    WIFI_COUNTRY_POLICY_AUTO,   /**< Country policy is auto, use the country info of AP to which the station is connected */
    WIFI_COUNTRY_POLICY_MANUAL, /**< Country policy is manual, always use the configured country info */
} wifi_country_policy_t;

typedef struct {
    char                  cc[3];   /**< country code string */
    uint8_t               schan;   /**< start channel */
    uint8_t               nchan;   /**< total channel number */
    int8_t                max_tx_power;   /**< maximum tx power */
    wifi_country_policy_t policy;  /**< country policy */
} wifi_country_t;

typedef struct {
    uint8_t bssid[6];                     /**< MAC address of AP */
    uint8_t ssid[33];                     /**< SSID of AP */
    uint8_t primary;                      /**< channel of AP */
    wifi_second_chan_t second;            /**< secondary channel of AP */
    int8_t  rssi;                         /**< signal strength of AP */
    int16_t freq_offset;                  /**< frequency offset of AP */
    wifi_auth_mode_t authmode;            /**< authmode of AP */
    wifi_cipher_type_t pairwise_cipher;   /**< pairwise cipher of AP */
    wifi_cipher_type_t group_cipher;      /**< group cipher of AP */
    wifi_ant_t ant;                       /**< antenna used to receive beacon from AP */
    uint32_t phy_11b: 1;                  /**< bit: 0 flag to identify if 11b mode is enabled or not */
    uint32_t phy_11g: 1;                  /**< bit: 1 flag to identify if 11g mode is enabled or not */
    uint32_t phy_11n: 1;                  /**< bit: 2 flag to identify if 11n mode is enabled or not */
    uint32_t phy_lr: 1;                   /**< bit: 3 flag to identify if low rate is enabled or not */
    uint32_t wps: 1;                      /**< bit: 4 flag to identify if WPS is supported or not */
    uint32_t reserved: 27;                /**< bit: 5..31 reserved */
    wifi_country_t country;               /**< country information of AP */
} wifi_ap_record_t;

typedef enum {
    WIFI_SCAN_TYPE_ACTIVE = 0,  /**< active scan */
    WIFI_SCAN_TYPE_PASSIVE,     /**< passive scan */
} wifi_scan_type_t;

typedef struct {
    uint32_t min;  /**< minimum active scan time per channel, units: millisecond */
    uint32_t max;  /**< maximum active scan time per channel, units: millisecond, values above 1500ms may
                                          cause station to disconnect from AP and are not recommended.  */
} wifi_active_scan_time_t;

typedef union {
    wifi_active_scan_time_t active;  /**< active scan time per channel, units: millisecond. */
    uint32_t passive;                /**< passive scan time per channel, units: millisecond, values above 1500ms may
                                          cause station to disconnect from AP and are not recommended. */
} wifi_scan_time_t;

typedef struct {
    uint8_t* ssid;               /**< SSID of AP */
    uint8_t* bssid;              /**< MAC address of AP */
    uint8_t channel;             /**< channel, scan the specific channel */
    bool show_hidden;            /**< enable to scan AP whose SSID is hidden */
    wifi_scan_type_t scan_type;  /**< scan type, active or passive */
    wifi_scan_time_t scan_time;  /**< scan time per channel */
} wifi_scan_config_t;

#else
#endif /* APP_TRACE_POSIX */

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

#ifdef APP_TRACE_POSIX

#define IP4ADDR_STRLEN_MAX  16

/*! WiFi stack initial configuration DUMMY parameters */
#define WIFI_INIT_CONFIG_DEFAULT_DUMMY() { \
    .event_handler = NULL, \
    .osi_funcs = NULL, \
    .qos_enable = 0,\
    .ampdu_rx_enable = 0,\
    .rx_ba_win = 0,\
    .rx_ampdu_buf_num = 5,\
    .rx_ampdu_buf_len = 256,\
    .rx_max_single_pkt_len = (1600 - 524),\
    .rx_buf_len = 524,\
    .amsdu_rx_enable = 0,\
    .rx_buf_num = 5,\
    .rx_pkt_num = 5,\
    .left_continuous_rx_buf_num = 5,\
    .tx_buf_num = 5,\
    .nvs_enable = 0,\
    .nano_enable = 0,\
    .wpa3_sae_enable = 0, \
    .magic = 0x1F2F3F4F\
};

#define WIFI_INIT_CONFIG_DEFAULT()	\
		WIFI_INIT_CONFIG_DEFAULT_DUMMY()

/** WiFi event declarations */
typedef enum wifi_event_t{
    WIFI_EVENT_WIFI_READY = 1,           /**< WiFi ready */
    WIFI_EVENT_SCAN_DONE,                /**< finish scanning AP */
    WIFI_EVENT_STA_START,                /**< station start */
    WIFI_EVENT_STA_STOP,                 /**< station stop */
    WIFI_EVENT_STA_CONNECTED,            /**< station connected to AP */
    WIFI_EVENT_STA_DISCONNECTED,         /**< station disconnected from AP */
    WIFI_EVENT_STA_AUTHMODE_CHANGE,      /**< the auth mode of AP connected by station changed */
    WIFI_EVENT_STA_BSS_RSSI_LOW,         /**< AP's RSSI crossed configured threshold */
    WIFI_EVENT_STA_WPS_ER_SUCCESS,       /**< station wps succeeds in enrollee mode */
    WIFI_EVENT_STA_WPS_ER_FAILED,        /**< station wps fails in enrollee mode */
    WIFI_EVENT_STA_WPS_ER_TIMEOUT,       /**< station wps timeout in enrollee mode */
    WIFI_EVENT_STA_WPS_ER_PIN,           /**< station wps pin code in enrollee mode */
    WIFI_EVENT_AP_START,                 /**< soft-AP start */
    WIFI_EVENT_AP_STOP,                  /**< soft-AP stop */
    WIFI_EVENT_AP_STACONNECTED,          /**< a station connected to soft-AP */
    WIFI_EVENT_AP_STADISCONNECTED,       /**< a station disconnected from soft-AP */
    WIFI_EVENT_AP_PROBEREQRECVED,        /**< Receive probe request packet in soft-AP interface */
    MAX_WIFI_EVENTS,
} wifi_event_t;

/** IP event declarations */
typedef enum ip_event_t{
    IP_EVENT_STA_GOT_IP = 1,               /*!< station got IP from connected AP */
    IP_EVENT_STA_LOST_IP,              /*!< station lost IP and the IP is reset to 0 */
    IP_EVENT_AP_STAIPASSIGNED,         /*!< soft-AP assign an IP to a connected station */
    IP_EVENT_GOT_IP6,                  /*!< station or ap or ethernet interface v6IP addr is preferred */
    MAX_IP_EVENTS,
} ip_event_t;

#else

#endif /* APP_TRACE_POSIX */

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*!
 * @brief Checks if wifi is connected
 * @param timeout_ms Timeout value in milliseconds
 * @return True if connected, false otherwise
 */
app_err_t wifi_port_connect(void);
app_err_t wifi_port_disconnect(void);

void wifi_port_netif_init(void);
app_err_t wifi_port_init(const wifi_init_config_t *p_config);
app_err_t wifi_port_deinit(void);
app_err_t wifi_port_set_mode(wifi_mode_t mode);
app_err_t wifi_port_set_config(wifi_interface_t interface, wifi_config_t *p_config);
app_err_t wifi_port_start(void);
app_err_t wifi_port_stop(void);
char *wifi_port_ip_to_str(const ip4_addr_t *addr);
app_err_t wifi_port_scan_get_ap_records(uint16_t *number, wifi_ap_record_t *ap_records);
app_err_t wifi_port_scan_start(const wifi_scan_config_t *config, bool block);

#ifdef __cplusplus
}
#endif /*  __cplusplus */
