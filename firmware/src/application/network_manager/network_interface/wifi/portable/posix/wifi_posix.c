///@file wifi_posix.c

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include "wifi_application_layer.h"
#include "event_loop_portable_layer.h"
#include "proprietary_utils.h"

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

app_err_t wifi_port_connect() {
	event_loop_posix_req_t wifi_req = {
		.event_base = WIFI_EVENT,
		.event_id = WIFI_EVENT_STA_CONNECTED,
	};
	(void) event_loop_posix_req(&wifi_req, MS(100));

	event_loop_posix_req_t ip_req = {
		.event_base = IP_EVENT,
		.event_id = IP_EVENT_STA_GOT_IP,
	};
	(void) event_loop_posix_req(&ip_req, MS(100));

	return APP_OK;
}

app_err_t wifi_port_disconnect(void) {
	return APP_OK;
}

void wifi_port_netif_init() {
	return;
}

app_err_t wifi_port_init(const wifi_init_config_t *p_config) {
	APP_LOG(LOG_INFO, FMT_NONE("WiFi initialized"));

	return APP_OK;
}

app_err_t wifi_port_deinit(void) {
	APP_LOG(LOG_INFO, FMT_NONE("WiFi deinitialized"));

	return APP_OK;
}

app_err_t wifi_port_set_mode(wifi_mode_t mode) {
	APP_LOG(LOG_INFO, FMT_ARGS("WiFi mode set to: %u", mode));

	return APP_OK;
}

app_err_t wifi_port_set_config(wifi_interface_t interface, wifi_config_t *p_config) {
	if (interface == WIFI_APP_LAYER_IF_STA) {
		APP_LOG(LOG_INFO, FMT_ARGS("WiFi station if configured for: \n\tSSID: %s\n\tPWD: %s",
															 p_config->sta.ssid,
															 p_config->sta.password));
	}

	return APP_OK;
}

app_err_t wifi_port_start(void) {
	APP_LOG(LOG_INFO, FMT_NONE("WiFi started"));

	event_loop_posix_req_t wifi_req = {
		.event_base = WIFI_EVENT,
		.event_id = WIFI_EVENT_STA_START,
	};
	(void) event_loop_posix_req(&wifi_req, MS(100));

	return APP_OK;
}

app_err_t wifi_port_stop(void) {
	APP_LOG(LOG_INFO, FMT_NONE("WiFi stopped"));

	return APP_OK;
}

char *wifi_port_ip_to_str(const ip4_addr_t *addr) {
	return "127.0.0.0";
}

app_err_t wifi_port_scan_get_ap_records(uint16_t *number, wifi_ap_record_t *ap_records) {
	return APP_OK;
}

app_err_t wifi_port_scan_start(const wifi_scan_config_t *config, bool block) {
	return APP_OK;
}
