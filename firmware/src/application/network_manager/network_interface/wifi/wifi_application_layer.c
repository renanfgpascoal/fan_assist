///@file wifi_application_layer.c

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include "wifi_application_layer.h"
#include "event_loop_portable_layer.h"
#include "proprietary_utils.h"

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

typedef enum wifi_app_layer_event_flag_t {
	WIFI_APP_LAYER_CONNECTED_FLAG = (1 << 0),
	WIFI_APP_LAYER_FAIL_FLAG = (1 << 1),
} wifi_app_layer_event_flag_t;

typedef enum wifi_app_layer_defs_t {
	SCAN_AP_LIMIT = 15,
	SCAN_ONE_AP_JSON_SIZE = 99,
	IP_INFO_JSON_SIZE = 159,
} wifi_app_layer_defs_t;

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

static void wifi_app_layer_safe_update_sta_ip(uint32_t ip, uint32_t timeout_ms);
static void wifi_app_layer_safe_update_ip_info_json(uint32_t timeout_ms);
static void wifi_app_layer_safe_update_scan_list_json(uint32_t timeout_ms);
static void wifi_app_layer_scan_filter_unique(wifi_ap_record_t *aplist, uint16_t *aps);
static void wifi_app_layer_safe_clear_scan_list_json(uint32_t timeout_ms) ;
static void wifi_app_layer_scan_list_done(uint32_t timeout_ms);
static void wifi_app_layer_event_handler(void *arg,
                                         app_event_base_t event_base,
                                         int32_t event_id,
                                         void *event_data);
static bool wifi_app_layer_interface_init(wifi_app_config_t *p_from,
                                          wifi_app_layer_mode_t to);
static bool wifi_app_layer_interface_deinit();
static bool wifi_app_layer_event_init();
static bool wifi_app_layer_event_deinit();

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

static SEMAPHORE_HANDLE_TYPE wifi_app_sem_handle[2] = { NULL, NULL };
static EVENT_FLAG_HANDLE_TYPE event_flags_handle = NULL;

static uint32_t sta_connect_retries = STA_CONNECT_MAX_RETRIES;
static wifi_ap_record_t *p_scan_records;

static char *p_sta_ip_addr = NULL;
static char *p_sta_ip_info_json = NULL;
static char *p_scan_list_json = NULL;

static uint16_t scan_ap_num = SCAN_AP_LIMIT;

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

static void wifi_app_layer_safe_update_sta_ip(uint32_t ip, uint32_t timeout_ms) {
	if (SEMAPHORE_TAKE(wifi_app_sem_handle[0], timeout_ms) == pdFALSE) {
		return;
	}

	ip4_addr_t ip4 = { .addr = ip };

	char *p_str_ip = wifi_port_ip_to_str(&ip4);

	(void) strncpy(p_sta_ip_addr, p_str_ip, IP4ADDR_STRLEN_MAX);

	APP_LOG(LOG_WARN, FMT_ARGS("Set STA IP String to: %s", p_sta_ip_addr));

	(void) SEMAPHORE_GIVE(wifi_app_sem_handle[0]);
	return;
}

static void wifi_app_layer_safe_update_ip_info_json(uint32_t timeout_ms) {
	if (SEMAPHORE_TAKE(wifi_app_sem_handle[1], timeout_ms) == pdFALSE) {
		return;
	}

	(void) SEMAPHORE_GIVE(wifi_app_sem_handle[1]);
	return;
}

static void wifi_app_layer_safe_update_scan_list_json(uint32_t timeout_ms) {
	if (SEMAPHORE_TAKE(wifi_app_sem_handle[1], timeout_ms) == pdFALSE) {
		return;
	}

	strcpy(p_scan_list_json, "[");

	const char oneap_str[] = ",\"chan\":%d,\"rssi\":%d,\"auth\":%d}%c\n";

	/* stack buffer to hold on to one AP until it's copied over to p_scan_list_json */
	char one_ap[SCAN_ONE_AP_JSON_SIZE];
	for (int i = 0; i < scan_ap_num; i++) {

		wifi_ap_record_t ap = p_scan_records[i];

		/* ssid needs to be json escaped. To save on heap memory it's directly printed at the correct address */
		strcat(p_scan_list_json, "{\"ssid\":");
//		json_print_string((unsigned char*) ap.ssid, (unsigned char*) (p_scan_list_json + strlen(p_scan_list_json)));

		/* print the rest of the json for this access point: no more string to escape */
		snprintf(one_ap, (size_t) SCAN_ONE_AP_JSON_SIZE, oneap_str, ap.primary, ap.rssi, ap.authmode,
		    i == scan_ap_num - 1 ? ']' : ',');

		/* add it to the list */
		strcat(p_scan_list_json, one_ap);
	}

	(void) SEMAPHORE_GIVE(wifi_app_sem_handle[1]);
	return;
}

static void wifi_app_layer_scan_filter_unique(wifi_ap_record_t *aplist, uint16_t *aps) {
	int total_unique;
	wifi_ap_record_t *first_free;
	total_unique = *aps;

	first_free = NULL;

	for (int i = 0; i < *aps - 1; i++) {
		wifi_ap_record_t *ap = &aplist[i];

		/* skip the previously removed APs */
		if (ap->ssid[0] == 0)
			continue;

		/* remove the identical SSID+authmodes */
		for (int j = i + 1; j < *aps; j++) {
			wifi_ap_record_t *ap1 = &aplist[j];
			if ((strcmp((const char*) ap->ssid, (const char*) ap1->ssid) == 0) && (ap->authmode == ap1->authmode)) { /* same SSID, different auth mode is skipped */
				/* save the rssi for the display */
				if ((ap1->rssi) > (ap->rssi))
					ap->rssi = ap1->rssi;
				/* clearing the record */
				memset(ap1, 0, sizeof(wifi_ap_record_t));
			}
		}
	}
	/* reorder the list so APs follow each other in the list */
	for (int i = 0; i < *aps; i++) {
		wifi_ap_record_t *ap = &aplist[i];
		/* skipping all that has no name */
		if (ap->ssid[0] == 0) {
			/* mark the first free slot */
			if (first_free == NULL)
				first_free = ap;
			total_unique--;
			continue;
		}
		if (first_free != NULL) {
			memcpy(first_free, ap, sizeof(wifi_ap_record_t));
			memset(ap, 0, sizeof(wifi_ap_record_t));
			/* find the next free slot */
			for (int j = 0; j < *aps; j++) {
				if (aplist[j].ssid[0] == 0) {
					first_free = &aplist[j];
					break;
				}
			}
		}
	}
	/* update the length of the list */
	*aps = total_unique;
}

static void wifi_app_layer_safe_clear_scan_list_json(uint32_t timeout_ms) {
	if (SEMAPHORE_TAKE(wifi_app_sem_handle[1], timeout_ms) == pdFALSE) {
		return;
	}

	strcpy(p_scan_list_json, "{}\n");

	(void) SEMAPHORE_GIVE(wifi_app_sem_handle[1]);
	return;
}

static void wifi_app_layer_scan_list_done(uint32_t timeout_ms) {
	(void) wifi_port_scan_get_ap_records(&scan_ap_num, p_scan_records);

	if (SEMAPHORE_TAKE(wifi_app_sem_handle[1], timeout_ms) == pdFALSE) {
		return;
	}

	wifi_app_layer_scan_filter_unique(p_scan_records, &scan_ap_num);

	(void) SEMAPHORE_GIVE(wifi_app_sem_handle[1]);

	wifi_app_layer_safe_update_scan_list_json(timeout_ms);

	APP_LOG(LOG_WARN, FMT_ARGS("%s", (char *)p_scan_records[1].ssid));

	return;
}

static void wifi_app_layer_event_handler(void *arg,
                                         app_event_base_t event_base,
                                         int32_t event_id,
                                         void *event_data) {
	CONTINUE_CONDITION(event_flags_handle != NULL,);

	if (strcmp(event_base, WIFI_EVENT) == 0) {
		switch(event_id) {
			/* Wifi host STA events */
			case WIFI_EVENT_STA_START: {
				APP_LOG(LOG_WARN, FMT_NONE("Wifi STA started!"));
				(void) wifi_port_connect();

				break;
			}
			case WIFI_EVENT_STA_STOP: {
				APP_LOG(LOG_WARN, FMT_NONE("Wifi STA stopped!"));

				break;
			}
			case WIFI_EVENT_STA_CONNECTED: {
				sta_connect_retries = STA_CONNECT_MAX_RETRIES;

				CLR_EVENT_FLAG(event_flags_handle, WIFI_APP_LAYER_FAIL_FLAG);
				break;
			}
			case WIFI_EVENT_STA_DISCONNECTED: {
				CLR_EVENT_FLAG(event_flags_handle, WIFI_APP_LAYER_CONNECTED_FLAG);

				if (sta_connect_retries > 0) {
					(void) wifi_port_connect();
					sta_connect_retries--;

					APP_LOG(LOG_INFO, FMT_NONE("Retry to connect to the AP"));
				} else {
					SET_EVENT_FLAG(event_flags_handle, WIFI_APP_LAYER_FAIL_FLAG);
				}

				APP_LOG(LOG_ERROR, FMT_NONE("Connect to the AP fail"));
				break;
			}
			/* Wifi host AP events */
			case WIFI_EVENT_AP_START: {
				APP_LOG(LOG_WARN, FMT_NONE("Wifi AP started!"));

				break;
			}
			case WIFI_EVENT_AP_STOP: {
				APP_LOG(LOG_WARN, FMT_NONE("Wifi AP stopped!"));

				break;
			}
			case WIFI_EVENT_SCAN_DONE: {
				wifi_app_layer_scan_list_done(MS(100));

				break;
			}
			/* Unhandled events */
			default: {
				APP_LOG(LOG_WARN, FMT_ARGS("Unhandled Wifi event (%u)", event_id));

				break;
			}
		}
	}

	if (strcmp(event_base, IP_EVENT) == 0) {
		switch(event_id) {
			case IP_EVENT_STA_GOT_IP: {
				SET_EVENT_FLAG(event_flags_handle, WIFI_APP_LAYER_CONNECTED_FLAG);

				ip_event_got_ip_t *event = (ip_event_got_ip_t*) event_data;

				APP_LOG(LOG_INFO,
				        FMT_ARGS("Got ip: %s", wifi_port_ip_to_str(&event->ip_info.ip)));

				wifi_app_layer_safe_update_sta_ip(event->ip_info.ip.addr, MS(100));

				break;
			}
			default: {
				APP_LOG(LOG_WARN, FMT_ARGS("Unhandled IP event (%u)", event_id));

				break;
			}
		}
	}

	return;
}

static bool wifi_app_layer_interface_init(wifi_app_config_t *p_from,
                                          wifi_app_layer_mode_t to) {
	DEVELOPMENT_ASSERT(p_from != NULL, false);

	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	if (wifi_port_init(&cfg) != APP_OK) {

		return false;
	}

	if (wifi_port_set_mode(to) != APP_OK) {

		return false;
	}

	if ((to == WIFI_APP_LAYER_MODE_AP) || (to == WIFI_APP_LAYER_MODE_APSTA)) {
		if (wifi_port_set_config(WIFI_APP_LAYER_IF_AP, p_from->p_ap) != APP_OK) {

			return false;
		}
	}

	if ((to == WIFI_APP_LAYER_MODE_STA) || (to == WIFI_APP_LAYER_MODE_APSTA)) {
		if (wifi_port_set_config(WIFI_APP_LAYER_IF_STA, p_from->p_sta) != APP_OK) {

			return false;
		}
	}

	return true;
}

static bool wifi_app_layer_interface_deinit() {
	return wifi_port_deinit() == APP_OK;
}

static bool wifi_app_layer_event_init() {
	event_flags_handle = CREATE_EVENT_FLAG();

	if (event_flags_handle == NULL) {
		return false;
	}

	if (event_loop_port_create_default() != APP_OK) {
		return false;
	}

	if (event_loop_port_handler_register(WIFI_EVENT,
	                                     APP_EVENT_ANY_ID,
	                                     &wifi_app_layer_event_handler,
	                                     NULL) != APP_OK) {
		return false;
	}

	return event_loop_port_handler_register(IP_EVENT,
	                                        APP_EVENT_ANY_ID,
	                                        &wifi_app_layer_event_handler,
	                                        NULL) == APP_OK;
}

static bool wifi_app_layer_event_deinit() {
	if (event_loop_port_handler_unregister(WIFI_EVENT,
	                                       APP_EVENT_ANY_ID,
	                                       &wifi_app_layer_event_handler) != APP_OK) {
		return false;
	}

	if (event_loop_port_handler_unregister(IP_EVENT,
	                                       APP_EVENT_ANY_ID,
	                                       &wifi_app_layer_event_handler) != APP_OK) {
		return false;
	}

	if (event_loop_port_delete_default() != APP_OK) {
		return false;
	}

	DELETE_EVENT_FLAG(event_flags_handle);
	event_flags_handle = NULL;

	return true;
}

char* wifi_app_layer_get_sta_ip_addr(uint32_t timeout_ms) {
	if (SEMAPHORE_TAKE(wifi_app_sem_handle[0], timeout_ms) == pdFALSE) {
		return NULL;
	}

	char *p_to = p_sta_ip_addr;

	(void) SEMAPHORE_GIVE(wifi_app_sem_handle[0]);

	return p_to;
}

char* wifi_app_layer_get_sta_ip_info(uint32_t timeout_ms) {
	if (SEMAPHORE_TAKE(wifi_app_sem_handle[1], timeout_ms) == pdFALSE) {
		return NULL;
	}

	char *p_to = p_sta_ip_info_json;

	(void) SEMAPHORE_GIVE(wifi_app_sem_handle[1]);

	return p_to;
}

char* wifi_app_layer_get_scan_list(uint32_t timeout_ms) {
	if (SEMAPHORE_TAKE(wifi_app_sem_handle[1], timeout_ms) == pdFALSE) {
		return NULL;
	}

	char *p_to = p_scan_list_json;

	(void) SEMAPHORE_GIVE(wifi_app_sem_handle[1]);

	return p_to;
}

bool wifi_app_layer_start_scan_ap(uint32_t timeout_ms) {
	wifi_scan_config_t scan_config = {
	  .ssid = 0,
	  .bssid = 0,
	  .channel = 0,
	  .show_hidden = true,
	};
	return wifi_port_scan_start(&scan_config, false) == APP_OK;
}

bool wifi_app_layer_is_connected(uint32_t timeout_ms) {
	CONTINUE_CONDITION(event_flags_handle != NULL, false);
	EVENT_FLAGS_TYPE bits = CHECK_ONE_EVENT_FLAG(event_flags_handle,
	                                             WIFI_APP_LAYER_CONNECTED_FLAG,
	                                             MS(timeout_ms));

	return bits & WIFI_APP_LAYER_CONNECTED_FLAG;
}

bool wifi_app_connect() {
	return wifi_port_connect() == APP_OK;
}

bool wifi_app_disconnect() {
	return wifi_port_disconnect() == APP_OK;
}

bool wifi_app_layer_init(wifi_app_config_t *p_from, wifi_app_layer_mode_t to) {
	wifi_port_netif_init();

	if (wifi_app_layer_event_init() == false) {

		return false;
	}

	if (wifi_app_layer_interface_init(p_from, to) == false) {

		return false;
	}

	for (uint32_t index = 0; index < ARRAY_SIZE(wifi_app_sem_handle); index++) {
		wifi_app_sem_handle[index] = SEMAPHORE_DYNAMIC();
		(void) SEMAPHORE_GIVE(wifi_app_sem_handle[index]);
	}


	p_sta_ip_addr = (char*) malloc(sizeof(char) * IP4ADDR_STRLEN_MAX);
	p_sta_ip_info_json = (char*) malloc(sizeof(char) * IP_INFO_JSON_SIZE);

	p_scan_records = (wifi_ap_record_t*) malloc(sizeof(wifi_ap_record_t) * SCAN_AP_LIMIT);
	p_scan_list_json = (char*) malloc(sizeof(char) * SCAN_AP_LIMIT * SCAN_ONE_AP_JSON_SIZE + 4); /* 4 bytes for json encapsulation of "[\n" and "]\0" */
	wifi_app_layer_safe_clear_scan_list_json(MS(100));

	return wifi_port_start() == APP_OK;
}

bool wifi_app_layer_deinit() {
	if (wifi_port_stop() != APP_OK) {
		return false;
	}

	if (wifi_app_layer_event_deinit() == false) {
		return false;
	}

	free(p_sta_ip_addr);
	p_sta_ip_addr = NULL;
	free(p_scan_records);
	p_scan_records = NULL;
	free(p_scan_list_json);
	p_scan_list_json = NULL;
	free(p_sta_ip_info_json);
	p_sta_ip_info_json = NULL;

	return wifi_app_layer_interface_deinit();
}
