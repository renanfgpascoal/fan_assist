///@file httpd_posix.c

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include "httpd_portable_layer.h"

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

app_err_t httpd_port_resp_set_status(httpd_port_req_t *p_req, const char *p_status) {
	return APP_OK;
}

app_err_t httpd_port_resp_set_type(httpd_port_req_t *p_req, const char *p_type) {
	return APP_OK;
}

app_err_t httpd_port_resp_set_hdr(httpd_port_req_t *p_req,
                                  const char *p_field,
                                  const char *p_value) {
	return APP_OK;
}

app_err_t httpd_port_resp_send(httpd_port_req_t *p_req,
                               const char *p_buf,
                               uint32_t buf_len) {
	return APP_OK;
}

uint32_t httpd_port_req_get_hdr_len(httpd_port_req_t *p_req, const char *p_field) {
	return 0;
}

app_err_t httpd_port_req_to_sockfd(httpd_port_req_t *p_req) {
	return APP_OK;
}

int32_t httpd_port_req_recv(httpd_port_req_t *p_req, char *p_buf, uint32_t buf_len) {
	return 0;
}

app_err_t httpd_port_sess_close(httpd_port_handle_t handle, int sockfd) {
	return APP_OK;
}

app_err_t httpd_port_register_uri_handler(httpd_port_handle_t handle,
                                          const httpd_port_uri_t *p_uri_handler) {
	return APP_OK;
}

app_err_t httpd_port_stop(httpd_port_handle_t handle) {
	return APP_OK;
}

app_err_t httpd_port_start(httpd_port_handle_t *p_handle,
                           const httpd_port_config_t *p_config) {
	return APP_OK;
}
