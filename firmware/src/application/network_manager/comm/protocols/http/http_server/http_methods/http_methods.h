/// @file network_mgr_application.h

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#include "application.h"
#include "httpd_portable_layer.h"

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*!
 * @brief Configures Application
 * @param timeout Internal value of timeout for task new request waiting
 */
HTTP_METHOD_HANDLER(http_method_get_index);
HTTP_METHOD_HANDLER(http_method_get_js);
HTTP_METHOD_HANDLER(http_method_get_css);
HTTP_METHOD_HANDLER(http_method_get_ap);
HTTP_METHOD_HANDLER(http_method_get_status);
HTTP_METHOD_HANDLER(http_method_post_connect);
HTTP_METHOD_HANDLER(http_method_delete_connect);

#ifdef __cplusplus
}
#endif /*  __cplusplus */
