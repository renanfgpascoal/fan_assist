// save some bytes
const gel = (e) => document.getElementById(e);

const wifi_div = gel("wifi");
const connect_div = gel("connect");
const connect_manual_div = gel("connect_manual");
const connect_wait_div = gel("connect-wait");
const connect_details_div = gel("connect-details");
const circuit_name = gel("circuit_name");
const description = gel("description");
const circuit_info_form = gel("circuit_info_form");
const ip_config_select = gel("ip_config_select");
const container_ip_params = gel("container_ip_params");
const select_dns_auto = gel("select_dns_auto");
const div_dns_primary = gel("dns_pri");
const div_dns_secondary = gel("dns_sec");
const addr_dns_primary = gel("pri_dns_address");
const addr_dns_secondary = gel("sec_dns_address");

function docReady(fn) {
  // see if DOM is already available
  if (
    document.readyState === "complete" ||
    document.readyState === "interactive"
  ) {
    // call on next available tick
    setTimeout(fn, 1);
  } else {
    document.addEventListener("DOMContentLoaded", fn);
  }
}

var selectedSSID = "";
var refreshAPInterval = null;
var checkStatusInterval = null;

function stopCheckStatusInterval() {
  if (checkStatusInterval != null) {
    clearInterval(checkStatusInterval);
    checkStatusInterval = null;
  }
}

function stopRefreshAPInterval() {
  if (refreshAPInterval != null) {
    clearInterval(refreshAPInterval);
    refreshAPInterval = null;
  }
}

function startCheckStatusInterval() {
  checkStatusInterval = setInterval(checkStatus, 950);
}

function startRefreshAPInterval() {
  refreshAPInterval = setInterval(refreshAP, 3800);
}

async function updateMAC(url = "mac.json") {
  try {
    await fetch(url).then(res => {
      if (res.ok) {
        res.json().then(data => {
          document.querySelector(
            '#mac'
          ).textContent = data.mac;
        })
      } else {
        console.log("mac.json bad reponse")
      }
    });
    
  } catch (e) {
    console.info("Was not able to fetch /mac.json");
  }
}

// Get the invalid_info_alert
var invalid_info_alert = document.getElementById("invalid_info_alert");

// When the user clicks anywhere outside of the invalid_info_alert, close it
window.onclick = function(event) {
  if (event.target == invalid_info_alert) {
    invalid_info_alert.style.display = "none";
  }
}

updateMAC();

docReady(async function () {

  var circuit_info_switch = gel("circuit_info_switch");
  circuit_info_form.style.display = "none";
  circuit_info_switch.addEventListener(
    "change",
    () => {
      if (circuit_info_switch.checked) {
        circuit_info_form.style.display = "flex";
      }else{
        circuit_info_form.style.display = "none";
      }
    },
    false
  );

  container_ip_params.style.display = "none";
  ip_config_select.addEventListener(
    "change",
    () => {
      if (ip_config_select.value == "dynamic") {
        container_ip_params.style.display = "none";
      }else{
        container_ip_params.style.display = "block";
      }
    },
    false
  );

  select_dns_auto.addEventListener(
    "change",
    () => {
      if (select_dns_auto.checked == true){
        div_dns_primary.style.display = "none";
        div_dns_secondary.style.display = "none";
      }else {
        div_dns_primary.style.display = "block";
        div_dns_secondary.style.display = "block";
      }
    },
    false
  );

  gel("wifi-status").addEventListener(
    "click",
    () => {
      wifi_div.style.display = "none";
      document.getElementById("connect-details").style.display = "block";
    },
    false
  );

  gel("manual_add").addEventListener(
    "click",
    (e) => {
      if (((circuit_name.value != "") && (description.value != "") || (circuit_info_form.style.display == "none")) && (isValidIpParams() == true)) {
        selectedSSID = e.target.innerText;
  
        gel("ssid-pwd").textContent = selectedSSID;
        wifi_div.style.display = "none";
        connect_manual_div.style.display = "block";
        connect_div.style.display = "none";
  
        gel("connect-success").display = "none";
        gel("connect-fail").display = "none";      
        gel("module-fail").display = "none";      
        if (gel("manual_ssid").value == "" && gel("manual_pwd").value == "") {
          gel("manual_join").disabled = true;
        }
      }else {        
        invalid_info_alert.style.display = "block";
      }

      if (circuit_name.value == ""){
        circuit_name.style.border = 'solid 1px red';
      }else {
        circuit_name.style.border = 'solid 1px #888';          
      }

      if (description.value == ""){
        description.style.border = 'solid 1px red';
      }else {
        description.style.border = 'solid 1px #888';
      }
    },
    false
  );

  gel("wifi_list").addEventListener(
    "click",
    (e) => {
      if ((circuit_name.value != "") && (description.value != "") && (isValidIpParams() == true) || (circuit_info_form.style.display == "none")) {
        selectedSSID = e.target.innerText;
        gel("ssid-pwd").textContent = selectedSSID;
        connect_div.style.display = "block";
        wifi_div.style.display = "none";
        if (gel("pwd").value == "") {
          gel("join").disabled = true;
        }
      }else {
        invalid_info_alert.style.display = "block";
      }

      if (circuit_name.value == ""){
        circuit_name.style.border = 'solid 1px red';
      }else {
        circuit_name.style.border = 'solid 1px #888';          
      }

      if (description.value == ""){
        description.style.border = 'solid 1px red';
      }else {
        description.style.border = 'solid 1px #888';
      }
    },
    false
  );

  gel("close_button_info_alert").addEventListener(
    "click",
    (e) => {
      invalid_info_alert.style.display = "none";
    },
    false
  );

  function cancel() {
    selectedSSID = "";
    connect_div.style.display = "none";
    connect_manual_div.style.display = "none";
    wifi_div.style.display = "block";
  }

  gel("cancel").addEventListener("click", cancel, false);

  gel("manual_cancel").addEventListener("click", cancel, false);

  gel("join").addEventListener("click", performConnect, false);

  gel("manual_join").addEventListener(
    "click",
    (e) => {
      performConnect("manual");
    },
    false
  );

   gel("ok-details").addEventListener(
    "click",
    () => {
      connect_details_div.style.display = "none";
      wifi_div.style.display = "block";
    },
    false
  );

  // gel("ok-credits").addEventListener(
  //   "click",
  //   () => {
  //     gel("credits").style.display = "none";
  //     gel("app").style.display = "block";
  //   },
  //   false
  // );

  // gel("acredits").addEventListener(
  //   "click",
  //   () => {
  //     event.preventDefault();
  //     gel("app").style.display = "none";
  //     gel("credits").style.display = "block";
  //   },
  //   false
  // );

  gel("ok-connect").addEventListener(
    "click",
    () => {
      connect_wait_div.style.display = "none";
      wifi_div.style.display = "block";
    },
    false
  );

  gel("disconnect").addEventListener(
    "click",
    () => {
      gel("diag-disconnect").style.display = "block";
      gel("connect-details-wrap").classList.add("blur");
    },
    false
  );

  gel("no-disconnect").addEventListener(
    "click",
    () => {
      gel("diag-disconnect").style.display = "none";
      gel("connect-details-wrap").classList.remove("blur");
    },
    false
  );

  gel("yes-disconnect").addEventListener("click", async () => {
    stopCheckStatusInterval();
    selectedSSID = "";

    document.getElementById("diag-disconnect").style.display = "none";
    gel("connect-details-wrap").classList.remove("blur");

    await fetch("connect.json", {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
      body: { timestamp: Date.now() },
    });

    startCheckStatusInterval();

    connect_details_div.style.display = "none";
    wifi_div.style.display = "block";
  });

  //first time the page loads: attempt get the connection status and start the wifi scan
  await refreshAP();
  startCheckStatusInterval();
  startRefreshAPInterval();
});

function ipMask(o) {
  setTimeout(function () {
    var last_pos = o.selectionStart;
    var v = mIpAddr(o.value, last_pos);
    if (v != o.value) {
      o.value = v;
      if (o.selectionStart != last_pos + 1) {
        o.setSelectionRange(last_pos-1, last_pos-1);
      }
    }
  }, 1);
}

function mIpAddr(v, c_pos) {
  var r = v;
  if ((!v[c_pos - 1].match(/\./)) && (!v[c_pos - 1].match(/\d/)) || (r.length > 15)) {
    return v.slice(0, c_pos - 1) + v.slice(c_pos);
  }
  else if (r.length > 11) {
    r = r.replace(/^(\d{3})[\.](\d{3})[\.](\d{3})[\.](\d{3}).*/, "$1.$2.$3.$4");
  }
  else if (r.length > 7) {
    r = r.replace(/^(\d{3})[\.](\d{3})[\.](\d{3})$/, "$1.$2.$3.");
  }
  else if (r.length > 4) {
    r = r.replace(/^(\d{3})[\.](\d{3})$/, "$1.$2.");
  }
  else {
    r = r.replace(/^(\d{3})$/, "$1.");
  }
  return r;
}

function getFormInputsValues(form) {
  var values_array = {};
  var elements = form.querySelectorAll("input");
  for (var i = 0; i < elements.length; ++i) {
    var element = elements[i];
    var name = element.name;
    var value = element.value;

    if (name) {
      values_array[name] = value;
    }
  }
  return values_array;
}

function setFormInputsValues(form, value) {
  var elements = form.querySelectorAll("input");
  for (var i = 0; i < elements.length; ++i) {
    elements[i].value = value;
   }
}

function isValidIpParams() {
  var elements = container_ip_params.querySelectorAll("input");
  const ip_regex = /^[^\.]\d{0,3}[\.]\d{0,3}[^\.][\.]\d{0,3}[^\.][\.]\d{0,3}[^\.]$/g;
  var ret = true;
  if (ip_config_select.value == "static") {
    for (var i = 0; i < elements.length; ++i) {
      if (elements[i].isEqualNode(select_dns_auto)) {
        if (select_dns_auto.checked == true) {
          i = i+2;
        }
        continue;
      }
      if ((elements[i].value == "0.0.0.0") || (elements[i].value.match(ip_regex) == null)) {
        elements[i].style.border = 'solid 1px red';
  
        ret = false;
      }else {
        elements[i].style.border = 'solid 1px #888';
      }
    }
  }

  return ret;
}

function ip2int(ip) {
  return ip.split('.').reverse().reduce(function(ipInt, octet) { return (ipInt<<8) + parseInt(octet, 10)}, 0) >>> 0;
}

function convertIpParamsToInt(ip_params) {
  ip_params["ipv4"] = ip2int(ip_params["ipv4"]);
  ip_params["gw_address"] = ip2int(ip_params["gw_address"]);
  ip_params["netmask_address"] = ip2int(ip_params["netmask_address"]);
  ip_params["pri_dns_address"] = ip2int(ip_params["pri_dns_address"]);
  ip_params["sec_dns_address"] = ip2int(ip_params["sec_dns_address"]);
  
  if(ip_params["ip_mode"] == "dynamic") {
    ip_params["ip_mode"] = 1;
    return;
  }
  ip_params["ip_mode"] = 0;
  return;
}

function buildPayloadJSON(ssid, pwd) {
  var json = {};
  json["type"] = 15;

  var circuit_info = {};
  circuit_info = getFormInputsValues(circuit_info_form);
  
  var wifi_cred = {};
  wifi_cred["ssid"] = ssid;
  wifi_cred["pwd"] = pwd;

  if (ip_config_select.value == "dynamic") {
    setFormInputsValues(container_ip_params, "0.0.0.0");
  }else if (select_dns_auto.checked == true) {
    addr_dns_primary.value = "0.0.0.0";
    addr_dns_secondary.value = "0.0.0.0";
  }

  var ip_params = {};
  ip_params = getFormInputsValues(container_ip_params);
  ip_params["ip_mode"] = ip_config_select.value;
  convertIpParamsToInt(ip_params);

  json["circuit_info"] = circuit_info;
  json["wifi_cred"] = wifi_cred;
  json["ip_params"] = ip_params;
  return JSON.stringify(json, null, 2);
}

async function showPwd(input) {
  if (input.id == "check_show_man_pwd") {
    var pwd = gel("manual_pwd");
    if (pwd.type == "password") {
      pwd.type = "text";
    }else {
      pwd.type = "password";
    }
  }
  if (input.id == "check_show_pwd") {
    var pwd = gel("pwd");
    if (pwd.type == "password") {
      pwd.type = "text";
    }else {
      pwd.type = "password";
    }
  }
}

async function noButtonIfEmpty(input) {
  var parent = input.parentElement;
  if (input.id == "manual_ssid" || input.id == "manual_pwd") {
    if (input.value == "") {
      if (!parent.getElementsByClassName("invalid_input")[0]) {
        var invalidlabel = document.createElement("LABEL");
        invalidlabel.setAttribute("class", "invalid_input")
        invalidlabel.innerHTML = "* INSIRA LETRAS, NÚMEROS, OU SÍMBOLOS!";
        parent.appendChild(invalidlabel);
      }
      gel("manual_join").disabled = true;
    }else {
      if (parent.getElementsByClassName("invalid_input")[0]) {
        parent.getElementsByClassName("invalid_input")[0].remove();
      }
      gel("manual_join").disabled = false;
    }
  }
  if (input.id == "pwd") {
    if (input.value == "") {
      if (!parent.getElementsByClassName("invalid_input")[0]) {
        var invalidlabel = document.createElement("LABEL");
        invalidlabel.setAttribute("class", "invalid_input")
        invalidlabel.innerHTML = "* INSIRA LETRAS, NÚMEROS, OU SÍMBOLOS!";
        parent.appendChild(invalidlabel);
      }
      gel("join").disabled = true;
    }else {
      if (parent.getElementsByClassName("invalid_input")[0]) {
        parent.getElementsByClassName("invalid_input")[0].remove();
      }
      gel("join").disabled = false;
    }
  }
}

async function fetchWithTimeout(resource, options) {
  const { timeout = 10000 } = options;
  
  const controller = new AbortController();
  const id = setTimeout(() => {
    controller.abort()
    throw new Error('fetch fail!')
  }, timeout);
  
  const response = await fetch(resource, {
    ...options,
    signal: controller.signal  
  });
  clearTimeout(id);

  return response;
}

const fetchRetry = async (n, url, options) => {
  let error;
  for (let i = 0; i < n; i++) {
      try {
        console.log("trying fetchWithTimeout");
        return await fetchWithTimeout(url, options);
      } catch (err) {
          error = err;
      }
      console.log("atempt: ", i);

  }
  return new Response(null, {"status" : 408});
};
function encode_utf8(s) {
  return unescape(encodeURIComponent(s));
}
async function performConnect(conntype) {
  //stop the status refresh. This prevents a race condition where a status
  //request would be refreshed with wrong ip info from a previous connection
  //and the request would automatically shows as succesful.
  stopCheckStatusInterval();

  //stop refreshing wifi list
  stopRefreshAPInterval();

  var pwd;
  if (conntype == "manual") {
    //Grab the manual SSID and PWD
    selectedSSID = gel("manual_ssid").value;
    pwd = gel("manual_pwd").value;
  } else {
    pwd = gel("pwd").value;
  }
  //reset connection
  gel("loading").style.display = "block";
  gel("connect-success").style.display = "none";
  gel("connect-fail").style.display = "none";
  gel("module-fail").style.display = "none";

  gel("ok-connect").disabled = true;
  gel("ssid-wait").textContent = selectedSSID;

  connect_div.style.display = "none";
  connect_manual_div.style.display = "none";
  connect_wait_div.style.display = "block";
  
  console.log(buildPayloadJSON(selectedSSID, pwd));

  var timeout_msg = setTimeout(function() {
    gel("loading").style.display = "none";
    gel("ok-connect").disabled = false;
    gel("module-fail").style.display = "block";
  }, 30000);

  await fetchRetry(3, "connect.json", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: buildPayloadJSON(selectedSSID, pwd)
  }).then(res => {
    if (res.ok) {
      clearTimeout(timeout_msg);
      console.log("connect.json SUCCESS");
    } else {
      clearTimeout(timeout_msg);
      gel("loading").style.display = "none";
      gel("connect-success").style.display = "none";
      gel("connect-fail").style.display = "none";
      gel("module-fail").style.display = "block";
      gel("ok-connect").disabled = false;
      console.log("connect.json FAIL");
    }
  });

  //now we can re-set the intervals regardless of result
  startCheckStatusInterval();
  startRefreshAPInterval();
}

function rssiToIcon(rssi) {
  if (rssi >= -60) {
    return "w0";
  } else if (rssi >= -67) {
    return "w1";
  } else if (rssi >= -75) {
    return "w2";
  } else {
    return "w3";
  }
}

async function refreshAP(url = "ap.json") {
  try {
    var res = await fetch(url);
    var access_points = await res.json();
    if (access_points.length > 0) {
      //sort by signal strength
      access_points.sort((a, b) => {
        var x = a["rssi"];
        var y = b["rssi"];
        return x < y ? 1 : x > y ? -1 : 0;
      });
      refreshAPHTML(access_points);
    }
  } catch (e) {
    console.info("Access points returned empty from /ap.json!");
  }
}

function refreshAPHTML(data) {
  var h = "";
  data.forEach(function (e, idx, array) {
    let ap_class = idx === array.length - 1 ? "" : " brdb";
    let rssicon = rssiToIcon(e.rssi);
    let auth = e.auth == 0 ? "" : "pw";
    h += `<div class="ape${ap_class}"><div class="${rssicon}"><div class="${auth}">${e.ssid}</div></div></div>\n`;
  });

  gel("wifi_list").innerHTML = h;
}


async function checkStatus(url = "status.json") {
  try {
    var response = await fetch(url);
    var data = await response.json();
    if (data && data.hasOwnProperty("ssid") && data["ssid"] != "") {
      if (data["ssid"] === selectedSSID) {
        // Attempting connection
        switch (data["urc"]) {
          case 0:
            console.info("Got connection!");
            document.querySelector(
              "#connected-to div div div span"
            ).textContent = data["ssid"];
            document.querySelector("#connect-details h1").textContent =
              data["ssid"];
            gel("ip").textContent = data["ip"];
            gel("netmask").textContent = data["netmask"];
            gel("gw").textContent = data["gw"];
            gel("wifi-status").style.display = "block";
            
            //update wait screen
            gel("loading").style.display = "none";
            gel("connect-success").style.display = "block";
            gel("connect-fail").style.display = "none";
            gel("module-fail").style.display = "none";

            //unlock the wait screen if needed
            gel("ok-connect").disabled = true;
            break;
          case 1:
            console.info("Connection attempt failed!");
            document.querySelector(
              "#connected-to div div div span"
            ).textContent = data["ssid"];
            document.querySelector("#connect-details h1").textContent =
              data["ssid"];
            gel("ip").textContent = "0.0.0.0";
            gel("netmask").textContent = "0.0.0.0";
            gel("gw").textContent = "0.0.0.0";

            //don't show any connection
            gel("wifi-status").display = "none";

            //unlock the wait screen
            gel("ok-connect").disabled = false;

            //update wait screen
            gel("loading").style.display = "none";
            gel("connect-fail").style.display = "block";
            gel("connect-success").style.display = "none";
            gel("module-fail").style.display = "none";
            break;
        }
      } else if (data.hasOwnProperty("urc") && data["urc"] === 0) {
        console.info("Connection established");
        //ESP32 is already connected to a wifi without having the user do anything
        if (
          gel("wifi-status").style.display == "" ||
          gel("wifi-status").style.display == "none"
        ) {
          document.querySelector("#connected-to div div div span").textContent =
            data["ssid"];
          document.querySelector("#connect-details h1").textContent =
            data["ssid"];
          gel("ip").textContent = data["ip"];
          gel("netmask").textContent = data["netmask"];
          gel("gw").textContent = data["gw"];
          gel("wifi-status").style.display = "block";
        }
      }
    } else if (data.hasOwnProperty("urc") && data["urc"] === 2) {
      console.log("Manual disconnect requested...");
      if (gel("wifi-status").style.display == "block") {
        gel("wifi-status").style.display = "none";
      }
    }
  } catch (e) {
    console.info("Was not able to fetch /status.json");
  }
}

