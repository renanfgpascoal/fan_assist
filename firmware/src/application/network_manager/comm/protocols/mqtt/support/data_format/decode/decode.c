///@file

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include "cJSON.h"

/* System functioning includes end */

/* Module functioning includes start */

#include "decode.h"
#include "proprietary_assert.h"

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

uint32_t decode_msg_op(char *p_from) {
	cJSON *p_obj = cJSON_Parse(p_from);
	CONTINUE_CONDITION(p_obj != NULL, -1)

	uint32_t op = 0;
	if (1) { /* capture operation type */
		cJSON *p_op = cJSON_GetObjectItem(p_obj, "op");
		CONTINUE_CONDITION(p_op != NULL, -1)

		op = (uint32_t)p_op->valueint;
	}

	cJSON_Delete(p_obj);
	return op;
}
