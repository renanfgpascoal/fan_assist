///@file network_mgr_application.c

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#include "network_mgr_application.h"
#include "soft_wdt_application.h"
#include "netif_application_layer.h"
#include "mqtt_client_application_layer.h"
#include "proprietary_utils.h"
#include "http_server.h"

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*!
 * @brief Application main RTOS task
 * @param p_arg Address of arguments passed on task execution
 */
static void network_mgr_app_task(void *p_arg);

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

static uint32_t wait_timeout = 0;
static TASK_HANDLE_TYPE task_handle = NULL;

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

static void network_mgr_app_task(void *p_arg) {
	bool is_connected = netif_app_layer_is_connected(MS(50));

	while (is_connected == false) {
		is_connected = netif_app_layer_is_connected(MS(50));
		APP_LOG(LOG_WARN, FMT_ARGS("Is connected?: [%u]", is_connected));
		RELATIVE_DELAY(MS(2000));
		soft_wdt_app_kick(NETWORK_MGR_APP_IDX);
	}

	if (mqtt_client_app_layer_init() == false) {
		APP_LOG(LOG_ERROR, FMT_NONE("Mqtt client init failed!"));
		return;
	}


//	RELATIVE_DELAY(MS(10000));
//	http_server_start(true);

	for (;;) {
//		is_connected = netif_app_layer_is_connected(MS(50));
//		APP_LOG(LOG_WARN, FMT_ARGS("Is connected?: [%u]", is_connected));
		RELATIVE_DELAY(MS(2000));
		soft_wdt_app_kick(NETWORK_MGR_APP_IDX);
	}

	return;
}

void network_mgr_app_cfg(uint32_t timeout_ms) {
	CONTINUE_CONDITION(task_handle == NULL, );

  wait_timeout = timeout_ms;

  wifi_config_t ap_config = {
  	.ap = {
			.ssid = WIFI_AP_DEFAULT_SSID,
			.password = WIFI_AP_DEFAULT_PWD,
			.ssid_len = strlen(WIFI_AP_DEFAULT_SSID),
			.channel = 6,
			.authmode = WIFI_AUTH_WPA2_PSK,
			.ssid_hidden = 0,
			.max_connection = 1,
			.beacon_interval = 100,
		}
  };

  wifi_config_t sta_config = {
  	.sta = {
			.ssid = WIFI_STA_DEFAULT_SSID,
			.password = WIFI_STA_DEFAULT_PWD
		}
  };

  netif_config_t config = {
  	.wifi_config = {
			.config = {
				.p_sta = &sta_config,
				.p_ap = &ap_config,
				},
			.mode = WIFI_MODE_APSTA,
		},
  };

	if (netif_app_layer_init(&config) == false) {
		APP_LOG(LOG_ERROR, FMT_NONE("Netif init failed!"));
	}

  return;
}

void network_mgr_app_run(void) {
	CONTINUE_CONDITION(task_handle == NULL, );

	APP_LOG(LOG_WARN, FMT_NONE("Starting network_mgr_app"));

	(void) TASK_DYNAMIC(network_mgr_app_task,
	                    NETWORK_MGR_APP_STACK_SIZE,
	                    NULL,
	                    NETWORK_MGR_APP_PRIORITY,
	                    &task_handle);
  return;
}

void network_mgr_app_kill(void) {
	CONTINUE_CONDITION(task_handle != NULL, );

	APP_LOG(LOG_WARN, FMT_NONE("Killing network_mgr_app"));

	(void) netif_app_layer_deinit();
	TASK_DELETE(task_handle);
	task_handle = NULL;

	return;
}

#if 0

bool network_mgr_app_req(type2_t type, uint32_t timeout_ms) {
  if (NOTIFY(&name_tcb, type, eSetValueWithOverwrite) != pdPASS) {
    return false;
  }

  if (timeout_ms == 0) {
    return true;
  }

  if (QUEUE_PULL(&name_tcb, NULL, timeout_ms) != pdPASS) {
    return false;
  }

  return true;
}

#else

#endif
