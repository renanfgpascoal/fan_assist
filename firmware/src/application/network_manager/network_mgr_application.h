/// @file network_mgr_application.h

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#include "application.h"

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*!
 * @brief Configures Application
 * @param timeout Internal value of timeout for task new request waiting
 */
void network_mgr_app_cfg(uint32_t timeout_ms);

/*!
 * @brief Generates Application main task
 * @param task_prio Priority number for main task of this application
 */
void network_mgr_app_run(void);

/*!
 * @brief Kills Application task
 */
void network_mgr_app_kill(void);

#if 0

/*!
 * @brief Performs a request to this application
 * @param type Request type to perform (in case data is passed, should be an structure address, example is given on source)
 * @param timeout_ms Timeout to perform request (if no data is expected in return should be zero, 0)
 * @return Status of the operation
 */
bool network_mgr_app_req(type2_t type, uint32_t timeout_ms);

#endif

#ifdef __cplusplus
}
#endif /*  __cplusplus */
