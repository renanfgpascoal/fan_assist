///@file

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

#define GET_KICK_BIT_MASK(FROM)	(1 << (FROM))

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#include "fan_control_application.h"
#include "soft_wdt_application.h"
#include "network_mgr_application.h"
#include "proprietary_utils.h"

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*!
 * @brief Application main RTOS task
 * @param p_arg Address of arguments passed on task execution
 */
static void soft_wdt_app_task(void *p_arg);

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

static app_info_t *p_app_info_list = NULL;
static EVENT_FLAG_HANDLE_TYPE soft_wdt_kick = NULL;
static uint32_t soft_wdt_wake_time = MS(5000);

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

static void soft_wdt_app_task(void *p_arg) {
	EVENT_FLAGS_TYPE soft_wdt_kick_bits = 0;
	TickType_t last_wake = GET_TICK();

	uint32_t soft_wdt_kick_mask = 0;
	for (uint32_t index = 0; index < APP_MAX_INDEXES; index++) {
		soft_wdt_kick_mask |= GET_KICK_BIT_MASK(index);
	}

	for (;;) {
		ABSOLUTE_DELAY(&last_wake, soft_wdt_wake_time);
		soft_wdt_kick_bits = WAIT_EVENT_FLAG(soft_wdt_kick,
																				 soft_wdt_kick_mask,
																				 pdTRUE,
																				 pdTRUE,
																				 0);
//		APP_LOG(LOG_WARN, FMT_ARGS("0x%08X", (uint32_t)soft_wdt_kick_bits));

		if (soft_wdt_kick_bits != soft_wdt_kick_mask) {
			for (app_index_t index = 0; index < APP_MAX_INDEXES; index++) {
				if (((soft_wdt_kick_bits) & GET_KICK_BIT_MASK(index)) != 0) {
					continue;
				}

				if (p_app_info_list[index].is_critical == true) {
					if (p_app_info_list[index].killed_times >= CRITICAL_TASK_MAX_KILLS) {

						APP_LOG(LOG_ERROR, FMT_ARGS("Restart system, reason -> task idx: %u", index));
						SYSTEM_RESTART();
					}
				}

				switch (index) {
					case NETWORK_MGR_APP_IDX: {
						network_mgr_app_kill();
						break;
					}
					case FAN_CONTROL_APP_IDX: {
						fan_control_app_kill();
						break;
					}
					default: {
						break;
					}
				}

				p_app_info_list[index].killed_times++;
			}
		}
	}

	return;
}

void soft_wdt_app_kick(app_index_t from) {
	SET_EVENT_FLAG(soft_wdt_kick, GET_KICK_BIT_MASK(from));

	return;
}

void soft_wdt_app_cfg(app_info_t *p_from, uint32_t wake_time) {
	DEVELOPMENT_ASSERT(p_from != NULL, );

	p_app_info_list = p_from;
	soft_wdt_wake_time = wake_time;
	soft_wdt_kick = CREATE_EVENT_FLAG();

	return;
}

void soft_wdt_app_run(void) {
	TASK_DYNAMIC(soft_wdt_app_task,
	             SOFT_WDT_APP_STACK_SIZE,
	             NULL,
	             SOFT_WDT_APP_PRIORITY,
	             NULL);

	return;
}

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */
