///@file

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#include <time.h>

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#include "fw_config.h"
#include "application.h"
#include "proprietary_assert.h"

#include "nvs.h"

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include "app_nvs.h"

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

typedef enum nvs_fs_tag_idx_t {
	FS_FILE_APP = 0,
	FS_FILE_BKP = 2,
} nvs_fs_tag_idx_t;

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*!
 * @brief Read a value, its crc-32 and check its integrity
 * @param from Handle of file
 * @param tag Header tag index
 * @param p_file Address of string holding file specific name
 * @return Value read, if success, -1 if else
 */
static uint32_t app_nvs_read_u32_value(nvs_handle_t from,
                                       nvs_fs_tag_idx_t tag,
                                       const char p_file[FS_BASE_SIZE]);

/*!
 * @brief Write a value and corresponding crc-32
 * @param from Value to write
 * @param to Handle of file
 * @param tag Header tag index
 * @param p_file Address of string holding file specific name
 * @return True if success, false else
 */
static bool app_nvs_write_u32_value(uint32_t from,
                                    nvs_handle_t to,
                                    nvs_fs_tag_idx_t tag,
                                    const char p_file[FS_BASE_SIZE]);

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

static const char fs_header[][FS_TAGS_SIZE + 1] = {
  [FS_FILE_APP] = "app",
  [FS_FILE_BKP] = "bckp",
};

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

static uint32_t app_nvs_read_u32_value(nvs_handle_t from,
                                       nvs_fs_tag_idx_t tag,
                                       const char p_file[FS_BASE_SIZE]) {
	DEVELOPMENT_ASSERT(p_file != NULL, -1);

	uint64_t value = -1;
	if (1) { /* format file name for value access */
		char name[FS_NAME_SIZE] = { 0 };

		(void) strncpy(&name[0], &fs_header[tag][0], sizeof(name));
		(void) strncpy(&name[FS_TAGS_SIZE], &p_file[0], FS_BASE_SIZE);

		if (nvs_get_u64(from, &name[0], &value) != ESP_OK) {
			return -1;
		}
	}

	return value;
}

static bool app_nvs_write_u32_value(uint32_t from,
                                    nvs_handle_t to,
                                    nvs_fs_tag_idx_t tag,
                                    const char p_file[FS_BASE_SIZE]) {
	DEVELOPMENT_ASSERT(p_file != NULL, false);

	if (1) { /* format file name for value access */
		char name[FS_NAME_SIZE] = { 0 };

		(void) strncpy(&name[0], &fs_header[tag][0], sizeof(name));
		(void) strncpy(&name[FS_TAGS_SIZE], &p_file[0], FS_BASE_SIZE);

		if (nvs_set_u64(to, &name[0], from) != ESP_OK) {
			return false;
		}
		APP_LOG(LOG_WARN, FMT_ARGS("Written file: %s", &name[0]));
	}
	return true;
}

uint32_t app_nvs_read_u32_with_backup(const char p_from[FS_BASE_SIZE]) {
	DEVELOPMENT_ASSERT(p_from != NULL, -1);

	nvs_handle_t handle = 0;
	if (nvs_open_from_partition("nvs", "nvs", NVS_READONLY, &handle) != ESP_OK) {
		return -1;
	}

	uint32_t value = -1;

	/* try read original value */
	value = app_nvs_read_u32_value(handle, FS_FILE_APP, p_from);
	if (value == -1) { /* try to read from backed-up value */
		value = app_nvs_read_u32_value(handle, FS_FILE_BKP, p_from);
	}

	nvs_close(handle);
	return value;
}

bool app_nvs_write_u32_with_backup(uint32_t from, const char p_to[FS_BASE_SIZE]) {
	DEVELOPMENT_ASSERT(p_to != NULL, false);

	nvs_handle_t handle = 0;
	if (nvs_open_from_partition("nvs", "nvs", NVS_READWRITE, &handle) != ESP_OK) {
		return false;
	}

	/* try write application file*/
	(void) app_nvs_write_u32_value(from, handle, FS_FILE_APP, p_to);
	/* try write backup file*/
	bool status = app_nvs_write_u32_value(from, handle, FS_FILE_BKP, p_to);

	if (nvs_commit(handle) != ESP_OK) {
		status = false;
	}

	nvs_close(handle);
	return status;
}
