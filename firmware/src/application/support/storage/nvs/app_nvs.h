/// @file

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#include "proprietary_utils.h"

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/* Post includes defines start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Post includes defines end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

/*! File System definitions */
typedef enum fs_defs_t {
	/*! Tags size (iose, bckp, crcv, crcb...) */
	FS_TAGS_SIZE = 4,
	/*! Fixed base names size (e.en-bs, e.re.rd...)*/
	FS_BASE_SIZE = 12,
	/*! Key/file name size */
	FS_NAME_SIZE = FS_BASE_SIZE + FS_TAGS_SIZE,
	/*! Variable file name size */
	FS_FILE_SIZE = FS_BASE_SIZE - FS_TAGS_SIZE,
} fs_defs_t;

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

/*!
 * @brief Reads an 64-bit data from NVS, and its backup in case of failure
 * @param p_from Address of string holding base file name
 * @return Valuer read if success, 0xffffffffffffffff (-1) if fail
 */
uint32_t app_nvs_read_u32_with_backup(const char p_from[FS_BASE_SIZE]);

/*!
 * @brief Writes an 64-bit data to NVS, and its backup
 * @param from Value to write
 * @param p_to Address of string holding base file name
 * @return True if success, False if else
 */
bool app_nvs_write_u32_with_backup(uint32_t from, const char p_to[FS_BASE_SIZE]);

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#ifdef __cplusplus
}
#endif /*  __cplusplus */
