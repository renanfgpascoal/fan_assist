/// @file event_loop_port.h

#pragma once

#ifdef __cplusplus
extern "C" {
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#ifdef APP_TRACE_POSIX

#else

#include "esp_event.h"

#endif /* APP_TRACE_POSIX */

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include "application.h"

/* Module functioning includes end */

/* Post include defines start */

#ifdef APP_TRACE_POSIX

#define APP_EVENT_ANY_ID       0

#else

#define APP_EVENT_ANY_ID       ESP_EVENT_ANY_ID

#endif /* APP_TRACE_POSIX */

/* Post include defines end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

#ifdef APP_TRACE_POSIX

/*! WiFi stack initial configuration DUMMY parameters */
typedef const char* app_event_base_t;
typedef void (*app_event_handler_t)(void* event_handler_arg,
																		app_event_base_t event_base,
																		int32_t event_id,
																		void* event_data); /**< function called when an event is posted to the queue */
typedef struct event_loop_posix_req_t {
	app_event_base_t event_base;
	int32_t event_id;
} event_loop_posix_req_t;

#else

typedef esp_event_base_t app_event_base_t;
typedef esp_event_handler_t app_event_handler_t;

#endif /* APP_TRACE_POSIX */
/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

#ifdef APP_TRACE_POSIX

#define WIFI_EVENT	"WIFI_EVENT"
#define IP_EVENT	"IP_EVENT"

#endif /* APP_TRACE_POSIX */

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*!
 * @brief Checks if wifi is connected
 * @param timeout_ms Timeout value in milliseconds
 * @return True if connected, false otherwise
 */
app_err_t event_loop_port_event_post(app_event_base_t event_base,
                                     int32_t event_id,
                                     void *p_event_data,
                                     size_t event_data_size,
                                     uint32_t timeout_ms);
app_err_t event_loop_port_create_default();
app_err_t event_loop_port_delete_default();
app_err_t event_loop_port_handler_register(app_event_base_t event_base,
                                           int32_t event_id,
                                           void *event_handler,
                                           void *event_handler_arg);
app_err_t event_loop_port_handler_unregister(app_event_base_t event_base,
                                             int32_t event_id,
                                             void *event_handler);

#ifdef APP_TRACE_POSIX

bool event_loop_posix_req(event_loop_posix_req_t *p_from, uint32_t timeout_ms);

#endif /* APP_TRACE_POSIX */

#ifdef __cplusplus
}
#endif /*  __cplusplus */
