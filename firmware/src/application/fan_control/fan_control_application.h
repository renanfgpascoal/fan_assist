/// @file

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif /*  __cplusplus */

/*******************************************************************
 * EXTERNED DEFINES
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

#include <stdint.h>
#include <stdbool.h>

/* System functioning includes end */

/* Module functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/*******************************************************************
 * EXTERNED TYPES
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

typedef enum fan_control_app_req_t {
	FAN_CONTROL_SPD_OFF_REQ,
	FAN_CONTROL_SPD_MIN_REQ,
	FAN_CONTROL_SPD_MED_REQ,
	FAN_CONTROL_SPD_MAX_REQ,
	FAN_CONTROL_OSC_OFF_REQ,
	FAN_CONTROL_OSC_ON_REQ,
	FAN_CONTROL_OSC_TOGGLE_REQ,
	FAN_CONTROL_REQ_MAX
} fan_control_app_req_t;

/*******************************************************************
 * EXTERNED DATA
 *******************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*******************************************************************
 * EXTERNED FUNCTIONS
 *******************************************************************/

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/*!
 * @brief Configures Application
 * @param p_configs Address of structure holding input configurations: list of queues to exchange data and list of tasks to notify
 * @param time Internal value of time for task self activation (if any)
 */
void fan_control_app_cfg(uint32_t timeout_ms);

/*!
 * @brief Generates Application main task
 * @param task_prio Priority number for main task of this application
 */
void fan_control_app_run(void);

void fan_control_app_kill(void);

void fan_control_app_req_from_isr(fan_control_app_req_t type);

/*!
 * @brief Performs a request to this application
 * @param type Request type to perform (in case data is passed, should be an structure address, example is given on source)
 * @param timeout_ms Timeout to perform request (if no data is expected in return should be zero, 0)
 * @return Status of the operation
 */
bool fan_control_app_req(fan_control_app_req_t type);

#ifdef __cplusplus
}
#endif /*  __cplusplus */
