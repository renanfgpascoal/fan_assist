///@file

/*******************************************************************
 * PRIVATE DEFINES
 *******************************************************************/

/* Module functioning defines start */

#ifdef UNIT_TESTS

#else

/*! @brief Hides internal function definitions when not in unit test environment */
#define STATIC    static

#endif /* UNIT_TESTS */

/* Module functioning defines end */

/*******************************************************************
 * INCLUDES
 *******************************************************************/

/* System functioning includes start */

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* System functioning includes end */

/* Module functioning includes start */

#include "application_template.h"

#ifdef UNIT_TESTS

#endif /* UNIT_TESTS */

/* Module functioning includes end */

/**************************************************************************************************
 * PRIVATE TYPES
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/**************************************************************************************************
 * PRIVATE PROTOTYPES
 ************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

/*!
 * @brief Application main RTOS task
 * @param p_arg Address of arguments passed on task execution
 */
static void name_task(void *p_arg);

/**************************************************************************************************
 * PRIVATE DATA
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

static uint32_t task_time = 0;
static req_res_queues_t queues = { NULL, NULL };
static StaticTask_t name_tcb;

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#ifdef UNIT_TESTS

#else /* UNIT_TESTS */

#endif /* UNIT_TESTS */

static void name_task(void *p_arg) {
	while(1) {
		RELATIVE_DELAY((uint32_t)-1);
	}

	return;
}

void name_app_cfg(uint32_t time) {
  /* optional: use notifications preferentially to queues */
  static type1_t storage_req_buffer[1];
  static StaticQueue_t name_req_queue;

  /* optional: if no data is sent from this task, don't use */
  static type1_t storage_res_buffer[1];
  static StaticQueue_t name_res_queue;

  name_task_time = time;

  name_queues.p_req_handle = QUEUE_STATIC(name_req_buffer, &name_req_queue);
  name_queues.p_res_handle = QUEUE_STATIC(name_res_buffer, &name_res_queue);

  return;
}

void name_app_run(uint32_t task_prio) {
  static uint8_t name_task_stack[NAME_APP_STACK_SIZE];

  TASK_STATIC(name_task, name_task_stack, NULL, task_prio, &name_tcb);
  return
}

#if 1

bool name_app_req(type2_t type, uint32_t timeout_ms) {
  if (NOTIFY(&name_tcb, type, eSetValueWithOverwrite) != pdPASS) {
    return false;
  }

  if (timeout_ms == 0) {
    return true;
  }

  if (QUEUE_PULL(&name_tcb, NULL, timeout_ms) != pdPASS) {
    return false;
  }

  return true;
}

#else

#endif
