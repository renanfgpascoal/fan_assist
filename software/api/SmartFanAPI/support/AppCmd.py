from support.AppComm import AppComm


class AppCmd():
    def __do_transaction(self, client: AppComm, route: str, req: dict) -> bool:
        # Sending request
        trans_id = client.transmit_msg(req, route)
        if (trans_id == ""):
            return False
        
        # Listening for response
        res = client.track_msg(trans_id, route, 30)
        if (res == {}):
            return False
        
        return True if (res["status"] == "success") else False
    
    def set_fan_pwr(self, client: AppComm, route: str, dev_id: str, pwr: int):
        req = {
            "dev_id": dev_id,
            "op_id": 0,
            "fan_pwr": pwr
        }
        
        return self.__do_transaction(client, route, req)

    def turn_on(self, client: AppComm, route: str, dev_id: str):
        req = {
            "dev_id": dev_id,
            "op_id": 0,
            "fan_pwr": 10
        }
        
        return self.__do_transaction(client, route, req)

    def turn_off(self, client: AppComm, route: str, dev_id: str):
        req = {
            "dev_id": dev_id,
            "op_id": 0,
            "fan_pwr": 0
        }
        
        return self.__do_transaction(client, route, req)
