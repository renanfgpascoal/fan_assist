from paho.mqtt import client as mqtt_client
import random, time, ssl

broker = "mqtt.flespi.io"
port = 8883
topic = "/topic/test2"
client_id = "python-mqtt-{}".format(random.randint(0, 100))
username = "FlespiToken 8WO1QAhHm97ElFYUsN7DMOCEFhmzuHistUh9ipFuoCXy5LinodO0SftlkDKEaRWr"
password = ""

def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if (rc == 0):
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)
    
    # Set Connecting Client ID
    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.tls_set("certs/mqtt_flespi_io.pem", tls_version=ssl.PROTOCOL_TLSv1_2)
    client.tls_insecure_set(True)
    client.connect(broker, port)
    
    return client

def publish(client):
    msg_count = 0
    while (True):
        time.sleep(1)
        msg = "messages: {}".format(msg_count)
        result = client.publish(topic, msg)

        # result: [0, 1]
        status = result[0]
        if (status == 0):
            print("Send '{}' to topic '{}'". format(msg, topic))
        else:
            print("Failed to send message to topic {}".format(topic))
        
        msg_count += 1

def run():
    client = connect_mqtt()
    client.loop_start()
    publish(client)

if (__name__ == '__main__'):
    run()