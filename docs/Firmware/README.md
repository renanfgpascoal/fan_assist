# Firmware Specs
This document describes specifications for Smart Plug Firmware.<br>
Showing firmware responsibilities, tasks and architecture.

## 1 - Goals
* Configure and connect to local Wi-Fi network;
  * Over host AP;
  * By web interface.
* Commute a relay:
  * Handling GPIO pins;
  * By web interface;
  * Open relay when laptop battery is full.

## 2 - Workflow
![workflow](../Images/firmware_workflow_diag_v0r0.png)

## 3 - Architecture
This firmware is made over a multitask preemptive realtime operational system (RTOS) and have a main task managing all others tasks to ensure device will not getting in a panic fail.

### 3.1 - Layers
![layers](../Images/firmware_layers_diag_v0r1.png)

#### 3.1.1 - Application (tasks)
##### a) Task monitor

| Specs | Description |
|-------|-------------|
| **Responsibility** | Monitors others tasks to decide if restart a non-critical task or entire system |
| **Caller** | Main |
| **Resources** | RTOS functions |
| **Interfaces** | Application |

##### b) Asynchronous actions

| Specs | Description |
|-------|-------------|
| **Responsibility** | Executes actions any time |
| **Caller** | Main |
| **Resources** | Commutation functions |
| **Interfaces** | Commutation |

##### c) Network manager

| Specs | Description |
|-------|-------------|
| **Responsibility** | Manages network configuration and operations |
| **Caller** | Main |
| **Resources** | Wi-Fi functions |
| **Interfaces** | Wi-Fi |

##### d) Remote communication

| Specs | Description |
|-------|-------------|
| **Responsibility** | Handles remote requests and responses |
| **Caller** | Main |
| **Resources** | Network connection and MQTT functions |
| **Interfaces** | MQTT |

##### f) HTTP server

| Specs | Description |
|-------|-------------|
| **Responsibility** | Provides a web application to browser |
| **Caller** | Main |
| **Resources** | Network connection and web application files |
| **Interfaces** |  |

#### 3.1.2 - Interface
##### a) Wi-Fi

| Specs | Description |
|-------|-------------|
| **Responsibility** | Provides Wi-Fi managing functions |
| **Consumer** | Network manager |
| **Resources** | Wi-Fi driver functions |
| **Interfaces** | FreeRTOS ESP8266 or POSIX |

##### b) MQTT

| Specs | Description |
|-------|-------------|
| **Responsibility** | Provides MQTT protocol functions |
| **Consumer** | Remote communication |
| **Resources** | MQTT API functions |
| **Interfaces** | FreeRTOS ESP8266 or POSIX |

##### c) Commutation

| Specs | Description |
|-------|-------------|
| **Responsibility** | Provides switch commutation functions |
| **Consumer** | Asynchronous actions |
| **Resources** | Relay commutation functions |
| **Interfaces** | Relay driver |

##### d) Application

| Specs | Description |
|-------|-------------|
| **Responsibility** | Provides RTOS functions |
| **Consumer** | Main, Network manager, Remote communication, Asynchronous actions, HTTP server |
| **Resources** | FreeRTOS functions |
| **Interfaces** | FreeRTOS ESP8266 or POSIX |

#### 3.1.3 - Drivers
##### a) Relay

| Specs | Description |
|-------|-------------|
| **Responsibility** | Provides relay physical commutation functions |
| **Consumer** | Commutation |
| **Resources** | GPIO functions |
| **Interfaces** | FreeRTOS ESP8266 or MOCKED |

#### 3.1.4 - Kernel
##### a) FreeRTOS POSIX

| Specs | Description |
|-------|-------------|
| **Responsibility** | Provides FreeRTOS POSIX port functions |
| **Consumer** | Application |
| **Resources** | FreeRTOS functions |
| **Interfaces** | FreeRTOS POSIX port |

##### b) FreeRTOS ESP8266 (IDF)

| Specs | Description |
|-------|-------------|
| **Responsibility** | Provides FreeRTOS ESP8266 port functions |
| **Consumer** | Application |
| **Resources** | FreeRTOS functions |
| **Interfaces** | FreeRTOS ESP8266 port |

#### 3.1.5 - Utilities
##### a) Web app

| Specs | Description |
|-------|-------------|
| **Responsibility** | Provides web interface files |
| **Consumer** | HTTP server |
| **Resources** | Html, CSS, Javascript |
| **Interfaces** | Html, CSS, Javascript files |

#### 3.1.6 - Configurations
##### a) Firmware

| Specs | Description |
|-------|-------------|
| **Responsibility** | Provides specific firmware project defines |
| **Consumer** | Main, Network manager, Remote communication, Asynchronous actions, HTTP server |

##### b) Hardware

| Specs | Description |
|-------|-------------|
| **Responsibility** | Provides specific hardware project defines |
| **Consumer** | Relay driver, FreeRTOS ESP8266 or POSIX |

## 4 - Tasks
### 4.1 - Task monitor
### 4.2 - Network manager
### 4.3 - Remote communication
### 4.4 - Asynchronous actions
### 4.5 - HTTP server
## 5 - Tests
### 5.1 - Unit tests
### 5.2 - Runtime simulation
### 5.3 - Device tests